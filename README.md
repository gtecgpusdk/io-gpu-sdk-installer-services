# io-gpu-sdk-installer-services v2019.1.0

> WUI Framework library focused on services and business logic for GPU SDK Installer.

## Requirements

This library does not have any special requirements but it depends on the 
[WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder). See the WUI Builder requirements before you build this project.

## Project build

The project build is fully automated. For more information about the project build, see the 
[WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder) documentation.

## Documentation

This project provides automatically-generated documentation in [TypeDoc](http://typedoc.org/) from the TypeScript source by running the 
`wui docs` command from the {projectRoot} folder.

> NOTE: The documentation is accessible also from the {projectRoot}/build/target/docs/index.html file after a successful creation.

## History

### v2019.1.0
WUI Core update. Usage of Vision SDK domain names.
### v2019.0.1
WUI Core update. Usage of new app loader. Usage of app features.
### v2019.0.0
Refactoring of namespaces. Usage of multi-hub configuration. Fixed creation of shortcuts.
### v2018.3.0
WUI Core update.
### v2018.1.1
Project settings and WUI Core update. Localization and branding bug fixes for Vision SDK fork. 
Added ability to register Vision SDK Installer as WIN app. Move install folder from temp to local appdata. Added install of mys2 from cache.
### v2018.1.0
Update of WUI Core supporting multithreading. Added installation protocol for Vision SDK Studio and its toolchain. 
Added ability to configure GPU SDK installer as fork for Vision Studio Installer.
### v1.2.0
Added support for L4.9.11 BSP. Migration to portable GIT. Update of SCR and change of history ordering. 
Added ability to load test configurations from cloud.
### v1.1.0
Convert of runtime config to JSONP. Fixed validation of OpenCV. Added detailed description for VTK records.
### v1.0.6
Distribution bug fixes. Added fixed versions for dependencies.
### v1.0.5
Update of selfextractor configuration.
### v1.0.4
Update of TypeScript syntax. Added graphics for splash screen. Configuration updates.
### v1.0.3
Fixed executable naming and added minor GUI updates
### v1.0.2
GUI updates and added more detailed info for better end-user experiences
### v1.0.1
WUI namespaces refactoring 
### v1.0.0
Initial release

## License

This software is owned or controlled by NXP Semiconductors. 
Use of this software is governed by the BSD-3-Clause License distributed with this material.
  
See the `LICENSE.txt` file distributed for more details.

---

Author Jakub Cieslar,
Copyright (c) 2016 [Freescale Semiconductor, Inc.](http://freescale.com/),
Copyright (c) 2017-2019 [NXP](http://nxp.com/)
