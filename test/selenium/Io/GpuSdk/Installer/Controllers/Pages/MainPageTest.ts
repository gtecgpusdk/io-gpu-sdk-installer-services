/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.GpuSdk.Installer.Services.Controllers.Pages {
    "use strict";
    import BaseViewer = Com.Wui.Framework.Gui.Primitives.BaseViewer;
    import MainPage = Io.GpuSdk.Installer.Gui.BaseInterface.Pages.MainPage;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import SeleniumTestRunner = Com.Wui.Framework.Commons.SeleniumTestRunner;

    export class MainPageTest extends SeleniumTestRunner {

        public __IgnoretestInstall() : void {
            this.driver.executeScript(() : string => {
                const ViewerManager : any = Com.Wui.Framework.Gui.ViewerManager;
                const ObjectValidator : any = Com.Wui.Framework.Commons.Utils.ObjectValidator;
                const MainPageViewer : any = Io.GpuSdk.Installer.Gui.BaseInterface.Viewers.Pages.MainPageViewer;

                let installButtonId : string = "";
                ViewerManager.getLoadedViewers().foreach(($viewer : BaseViewer) : void => {
                    if (!ObjectValidator.IsEmptyOrNull($viewer.getInstance()) && $viewer.IsTypeOf(MainPageViewer)) {
                        const instance : MainPage = <MainPage>$viewer.getInstance();
                        installButtonId = instance.initialScreenPanel.installButton.Id();
                    }
                });
                return installButtonId;
            }).then(($value : string) : void => {
                LogIt.Debug($value);
                this.driver.findElement(this.by.id($value)).click().then(() : void => {
                    this.driver.quit();
                });
            });
        }

        protected setUp() : void {
            this.driver.get("file:///" + this.getAbsoluteRoot() + "/build/target/index.html?debug=JRESimulator");
        }
    }
}
