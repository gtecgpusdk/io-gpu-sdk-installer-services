/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.GpuSdk.Installer.Services.Utils {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import EnvironmentArgs = Com.Wui.Framework.Commons.EnvironmentArgs;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import FileSystemHandlerConnector = Com.Wui.Framework.Services.Connectors.FileSystemHandlerConnector;
    import IInstallationUtils = Com.Wui.Framework.Services.Interfaces.DAO.IInstallationUtils;
    import InstallationRecipeDAO = Com.Wui.Framework.Services.DAO.InstallationRecipeDAO;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import ShortcutOptions = Com.Wui.Framework.Services.Connectors.ShortcutOptions;
    import FileSystemDownloadOptions = Com.Wui.Framework.Services.Connectors.FileSystemDownloadOptions;
    import HttpMethodType = Com.Wui.Framework.Commons.Enums.HttpMethodType;
    import WebServiceClientFactory = Com.Wui.Framework.Services.WebServiceApi.WebServiceClientFactory;
    import ObjectEncoder = Com.Wui.Framework.Commons.Utils.ObjectEncoder;
    import TerminalConnector = Com.Wui.Framework.Services.Connectors.TerminalConnector;
    import WindowHandlerConnector = Com.Wui.Framework.Services.Connectors.WindowHandlerConnector;
    import ProgressEventArgs = Com.Wui.Framework.Commons.Events.Args.ProgressEventArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IWebServiceClient = Com.Wui.Framework.Services.Interfaces.IWebServiceClient;
    import EventsManager = Com.Wui.Framework.UserControls.Events.EventsManager;
    import UpdatesManagerConnector = Com.Wui.Framework.Services.Connectors.UpdatesManagerConnector;

    export class InstallerAppManager extends Com.Wui.Framework.Commons.Primitives.BaseObject {
        public static APP_SIZE : number = 103 * 1024 * 1024;

        private fileSystem : FileSystemHandlerConnector;
        private terminal : TerminalConnector;
        private utils : IInstallationUtils;
        private env : EnvironmentArgs;
        private shortcutOptions : ShortcutOptions;
        private readonly appName : string;
        private readonly relativePath : string;
        private installPath : string;
        private installerPath : string;
        private launcherPath : string;
        private callback : ($success : boolean, $message? : string) => void;
        private restService : UpdatesManagerConnector;
        private startMenuShortcutEnabled : boolean;
        private readonly isVisionSDK : boolean;
        private events : EventsManager;

        constructor($dao : InstallationRecipeDAO, $isVisionSDK : boolean = false) {
            super();

            this.fileSystem = $dao.ConfigurationLibrary().fileSystem;
            this.terminal = $dao.ConfigurationLibrary().terminal;
            this.utils = $dao.getConfigurationInstance().utils;
            this.env = Loader.getInstance().getEnvironmentArgs();

            this.appName = "GPU SDK Installer";
            this.relativePath = "NXP/GPU SDK";
            this.installPath = "NXP/GPU_SDK";
            this.shortcutOptions = <ShortcutOptions>{
                desc      : "NXP GPU SDK Installer focused on GPU enablement",
                icon      : this.installerPath + "/target/resource/graphics/GpuSdk.ico",
                workingDir: this.installerPath
            };
            this.restService = new UpdatesManagerConnector();
            this.isVisionSDK = $isVisionSDK;
            this.events = Loader.getInstance().getHttpResolver().getEvents();
        }

        public IsInstalled() : IInstalledPromise {
            const callback : any = {
                installedThen($status : boolean) : void {
                    // declare default callback
                }
            };
            this.utils.getRegisterValue(this.isVisionSDK ?
                "HKCU/Software/NXP/VisionSDK/Installer" : "HKLM/SOFTWARE/NXP/GPU SDK/" + this.env.getAppName(), "CurrentVersion",
                ($value : string) : void => {
                    if (!ObjectValidator.IsEmptyOrNull($value)) {
                        callback.installedThen(!StringUtils.VersionIsLower($value, this.env.getProjectVersion()));
                    } else {
                        callback.installedThen(false);
                    }
                });
            return {
                Then($callback : ($status : boolean) => void) : void {
                    callback.installedThen = $callback;
                }
            };
        }

        public getInstalledBSPVersion() : IBSPVersionPromise {
            const callback : any = {
                versionThen($version : string) : void {
                    // declare default callback
                }
            };
            if (this.isVisionSDK) {
                this.events.FireAsynchronousMethod(() : void => {
                    callback.versionThen("VisionSDKStudio");
                });
            } else {
                this.utils.getRegisterValue("HKLM/SOFTWARE/" + this.relativePath + "/" + this.env.getAppName(), "BSP",
                    ($value : string) : void => {
                        callback.versionThen((<any>StringUtils.Remove($value, "\\n")).trim());
                    });
            }
            return {
                Then($callback : ($version : string) => void) : void {
                    callback.versionThen = $callback;
                }
            };
        }

        public Install($applicationRootPath : string, $bspVersion : string, $desktopShortcut : boolean = true,
                       $startMenuShortcut : boolean = true) : IInstallUnistallPromise {
            if (this.isVisionSDK) {
                const launcherPath : string = $applicationRootPath + "/" + this.env.getAppName() + ".exe";
                this.utils.RegisterApp(false, "NXP/VisionSDK/Installer", this.env.getProjectVersion(), launcherPath,
                    ($status : boolean) : void => {
                        if ($status) {
                            const regKey : string = "HKCU/Software/Microsoft/Windows/CurrentVersion/Uninstall/VisionSDKStudio";
                            this.utils.RegisterValueExists(regKey, "UninstallPath", ($status : boolean) : void => {
                                if ($status) {
                                    this.utils.setRegisterValues([
                                        [regKey, "ModifyPath", StringUtils.Replace(launcherPath + "/", "/", "\\")],
                                        [regKey, "UninstallPath", StringUtils.Replace($applicationRootPath + "/", "/", "\\")],
                                        [regKey, "UninstallString", "\"" + StringUtils.Replace(launcherPath, "/", "\\") + "\""]
                                    ], ($status : boolean) : void => {
                                        if ($status) {
                                            this.callback(true);
                                        } else {
                                            this.callback($status, "Unable to register application uninstaller.");
                                        }
                                    });
                                } else {
                                    this.callback(true);
                                }
                            });
                        } else {
                            ExceptionsManager.Throw(this.getClassName(), "Unable to register application.");
                        }
                    });
            } else {
                this.startMenuShortcutEnabled = $startMenuShortcut;
                this.utils.getSystemDrive(($value : string | boolean) : void => {
                    if ($value !== false && !ObjectValidator.IsEmptyOrNull($value)) {
                        if (!StringUtils.StartsWith(this.installPath, <string>$value)) {
                            this.installPath = $value + "/" + this.installPath;
                        }
                        this.installerPath = $applicationRootPath;
                        this.shortcutOptions.icon = this.installerPath + "/target/resource/graphics/GpuSdk.ico";
                        this.shortcutOptions.workingDir = this.installerPath;
                        this.launcherPath = this.installerPath + "/" + this.env.getAppName() + ".exe";

                        this.fileSystem.Exists(this.launcherPath).Then(($status : boolean) : void => {
                            if (!$status && StringUtils.Contains(this.launcherPath, "/build/target/")) {
                                this.launcherPath = this.installerPath + "/build/target/target/index.html";
                            }
                            this.utils.RegisterApp(
                                true,
                                this.relativePath + "/" + this.env.getAppName(),
                                this.env.getProjectVersion(),
                                this.installerPath,
                                ($status : boolean) : void => {
                                    if ($status) {
                                        this.utils.setRegisterValue(
                                            "HKLM/SOFTWARE/" + this.relativePath + "/" + this.env.getAppName(),
                                            "BSP",
                                            $bspVersion,
                                            ($status : boolean) : void => {
                                                if ($status) {
                                                    if ($desktopShortcut) {
                                                        this.createDesktopShortcut();
                                                    } else {
                                                        this.getDesktopShortcutPath(($path : string) : void => {
                                                            this.fileSystem.Delete($path).Then(() : void => {
                                                                this.createInstallerShortcut();
                                                            });
                                                        });
                                                    }
                                                } else {
                                                    ExceptionsManager.Throw(this.getClassName(), "Unable to register BSP version.");
                                                }
                                            });
                                    } else {
                                        ExceptionsManager.Throw(this.getClassName(), "Unable to register application.");
                                    }
                                });
                        });
                    } else {
                        ExceptionsManager.Throw(this.getClassName(), "Unable to get System Drive.");
                    }
                });
            }
            return {
                Then: ($callback : ($success : boolean, $message? : string) => void) : void => {
                    this.callback = $callback;
                }
            };
        }

        public Uninstall() : IInstallUnistallPromise {
            if (this.isVisionSDK) {
                this.utils.RemoveRegisterKey("HKCU/Software/NXP/VisionSDK/Installer", null,
                    ($status : boolean) : void => {
                        if ($status) {
                            this.callback(true);
                        } else {
                            this.callback($status, "Unable to remove registration of the application.");
                        }
                    });
            } else {
                this.utils.RemoveRegisterKey("HKLM/SOFTWARE/" + this.relativePath + "/" + this.env.getAppName(), null,
                    ($status : boolean) : void => {
                        if ($status) {
                            this.utils.RemoveRegisterKey(
                                "HKLM/SOFTWARE/Microsoft/Windows/CurrentVersion/Uninstall/" + this.env.getAppName(), null,
                                ($status : boolean) : void => {
                                    if ($status) {
                                        this.removeShortcuts();
                                    } else {
                                        this.callback($status, "Unable to remove registration of the application.");
                                    }
                                });
                        } else {
                            this.callback($status, "Unable to remove registration of the application.");
                        }
                    });
            }
            return {
                Then: ($callback : ($success : boolean, $message? : string) => void) : void => {
                    this.callback = $callback;
                }
            };
        }

        public Selfinstall() : ISelfinstallPromise {
            const callbacks : any = {
                onChange($args : ProgressEventArgs) : void {
                    // declare default callback
                },
                onValidate($success : boolean) : void {
                    // declare default callback
                },
                selfinstallThen($success : boolean, $message? : string) : void {
                    // declare default callback
                }
            };

            const client : IWebServiceClient = WebServiceClientFactory.getClientById(this.restService.getId());
            client.StopCommunication();
            client.StartCommunication();

            this.restService
                .UpdateExists(
                    this.env.getProjectName(), this.env.getReleaseName(), this.env.getPlatform(), this.env.getProjectVersion(),
                    new Date(this.env.getBuildTime()).getTime())
                .Then(($status : boolean) : void => {
                    callbacks.onValidate($status);
                    if ($status) {
                        this.fileSystem
                            .Download(<FileSystemDownloadOptions>{
                                body   : "jsonData=" + JSON.stringify({
                                    data  : ObjectEncoder.Base64(JSON.stringify({
                                        args: ObjectEncoder.Base64(JSON.stringify([
                                            this.env.getProjectName(), this.env.getReleaseName(), this.env.getPlatform()
                                        ])),
                                        name: "Com.Wui.Framework.Rest.Services.Utils.SelfupdatesManager.Download"
                                    })),
                                    id    : new Date().getTime(),
                                    origin: "http://localhost",
                                    token : "",
                                    type  : "LiveContentWrapper.InvokeMethod"
                                }),
                                headers: {
                                    "charset"     : "UTF-8",
                                    "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
                                    "user-agent"  : Loader.getInstance().getHttpManager().getRequest().getUserAgent()
                                },
                                method : HttpMethodType.POST,
                                url    : WebServiceClientFactory.getClientById(this.restService.getId()).getServerUrl()
                            })
                            .OnChange(($eventArgs : ProgressEventArgs) : void => {
                                callbacks.onChange($eventArgs);
                            })
                            .Then(($downloadPath : string) : void => {
                                const eventArgs : ProgressEventArgs = new ProgressEventArgs();
                                eventArgs.CurrentValue(-1);
                                callbacks.onChange(eventArgs);
                                this.utils.getLocalAppDataPath(($localPath : string) : void => {
                                    const projectFolder : string = this.isVisionSDK ? "Vision SDK Installer" : this.env.getProjectName();
                                    const destinationPath : string = $localPath + "/" + projectFolder + "-" + new Date().getTime();
                                    this.fileSystem.Unpack($downloadPath, <any>{
                                        output: destinationPath
                                    }).Then(() : void => {
                                        this.fileSystem.Delete($downloadPath).Then(() : void => {
                                            this.fileSystem.Exists(destinationPath).Then(($status : boolean) : void => {
                                                if ($status) {
                                                    callbacks.selfinstallThen(true);
                                                    const launchApp : any = () : void => {
                                                        const windowManager : WindowHandlerConnector = new WindowHandlerConnector();
                                                        windowManager.Hide().Then(() : void => {
                                                            this.terminal
                                                                .Execute(this.env.getAppName() + ".exe", [], destinationPath)
                                                                .Then(($exitCode : number) : void => {
                                                                    if (ObjectValidator.IsInteger($exitCode)) {
                                                                        if ($exitCode === 0) {
                                                                            windowManager.Close();
                                                                        } else {
                                                                            windowManager.Show().Then(() : void => {
                                                                                ExceptionsManager.Throw(this.getClassName(),
                                                                                    "Unable to launch updated version.");
                                                                            });
                                                                        }
                                                                    }
                                                                });
                                                        });
                                                    };
                                                    this.IsInstalled().Then(($status : boolean) : void => {
                                                        if ($status) {
                                                            this.getInstalledBSPVersion().Then(($value : string) : void => {
                                                                this.getDesktopShortcutPath(($path : string) : void => {
                                                                    this.fileSystem.Exists($path)
                                                                        .Then(($desktopExists : boolean) : void => {
                                                                            this.getStartMenuShortcutPath(($path : string) : void => {
                                                                                this.fileSystem.Exists($path)
                                                                                    .Then(($startMenuExists : boolean) : void => {
                                                                                        this.Install(
                                                                                            destinationPath, $value,
                                                                                            $desktopExists, $startMenuExists)
                                                                                            .Then(($status : boolean,
                                                                                                   $message? : string) : void => {
                                                                                                if ($status) {
                                                                                                    launchApp();
                                                                                                } else {
                                                                                                    callbacks.selfinstallThen(false,
                                                                                                        $message);
                                                                                                }
                                                                                            });
                                                                                    });
                                                                            });
                                                                        });
                                                                });
                                                            });
                                                        } else {
                                                            launchApp();
                                                        }
                                                    });
                                                } else {
                                                    callbacks.selfinstallThen(false, "Unable to prepare selfinstall package.");
                                                }
                                            });
                                        });
                                    });
                                });
                            });
                    }
                });
            const promise : ISelfinstallPromise = {
                OnChange($callback : ($args : ProgressEventArgs) => void) : IInstallUnistallPromise {
                    callbacks.onChange = $callback;
                    return promise;
                },
                OnValidate($callback : ($success : boolean) => void) : ISelfinstallPromise {
                    callbacks.onValidate = $callback;
                    return promise;
                },
                Then($callback : ($success : boolean, $message? : string) => void) : void {
                    callbacks.selfinstallThen = $callback;
                }
            };
            return promise;
        }

        private createDesktopShortcut() : void {
            this.getDesktopShortcutPath(($path : string) : void => {
                this.fileSystem
                    .CreateShortcut(this.installPath, $path, this.shortcutOptions)
                    .Then(($status : boolean) : void => {
                        if ($status) {
                            this.createInstallerShortcut();
                        } else {
                            ExceptionsManager.Throw(this.getClassName(), "Unable to create desktop shortcut.");
                        }
                    });
            });
        }

        private createInstallerShortcut() : void {
            this.getInstallerShortcutPath(($path : string) : void => {
                this.fileSystem
                    .CreateShortcut(this.launcherPath, $path, this.shortcutOptions)
                    .Then(($status : boolean) : void => {
                        if ($status) {
                            if (this.startMenuShortcutEnabled) {
                                this.createStartMenuShortcut();
                            } else {
                                this.getStartMenuShortcutPath(($path : string) : void => {
                                    this.fileSystem.Delete($path).Then(() : void => {
                                        this.registerUninstall();
                                    });
                                });
                            }
                        } else {
                            ExceptionsManager.Throw(this.getClassName(), "Unable to create installer shortcut.");
                        }
                    });
            });
        }

        private createStartMenuShortcut() : void {
            this.getStartMenuShortcutPath(($path : string) : void => {
                this.fileSystem
                    .CreateShortcut(this.launcherPath, $path, this.shortcutOptions)
                    .Then(($status : boolean) : void => {
                        if ($status) {
                            this.registerUninstall();
                        } else {
                            ExceptionsManager.Throw(
                                this.getClassName(), "Unable to create start menu shortcut.");
                        }
                    });
            });
        }

        private removeShortcuts() : void {
            this.getDesktopShortcutPath(($path : string) : void => {
                this.fileSystem.Delete($path).Then(($status : boolean) : void => {
                    if ($status) {
                        this.getStartMenuShortcutPath(($path : string) : void => {
                            this.fileSystem.Delete($path).Then(($status : boolean) : void => {
                                if ($status) {
                                    this.getInstallerShortcutPath(($path : string) : void => {
                                        this.fileSystem.Delete($path).Then(($status : boolean) : void => {
                                            if ($status) {
                                                this.utils.getStartMenuPath(true, ($path : string) : void => {
                                                    this.fileSystem.IsEmpty($path + "/" + this.relativePath)
                                                        .Then(($status : boolean) : void => {
                                                            if ($status) {
                                                                this.fileSystem.Delete($path + "/" + this.relativePath).Then(() : void => {
                                                                    this.fileSystem.Delete($path + "/NXP").Then(() : void => {
                                                                        this.callback(true);
                                                                    });
                                                                });
                                                            } else {
                                                                this.callback(true);
                                                            }
                                                        });
                                                });
                                            } else {
                                                ExceptionsManager.Throw(
                                                    this.getClassName(), "Unable to remove installer shortcut.");
                                            }
                                        });
                                    });
                                } else {
                                    ExceptionsManager.Throw(this.getClassName(), "Unable to remove start menu shortcut.");
                                }
                            });
                        });
                    } else {
                        ExceptionsManager.Throw(this.getClassName(), "Unable to remove desktop shortcut.");
                    }
                });
            });
        }

        private registerUninstall() : void {
            const regKey : string = "HKLM/SOFTWARE/Microsoft/Windows/CurrentVersion/Uninstall/" + this.env.getAppName();
            this.utils.setRegisterValues([
                [regKey, "DisplayIcon", StringUtils.Replace(this.shortcutOptions.icon, "/", "\\")],
                [regKey, "DisplayName", this.appName],
                [regKey, "DisplayVersion", this.env.getProjectVersion()],
                [regKey, "EstimatedSize", <any>(InstallerAppManager.APP_SIZE / 1024)],
                [regKey, "Publisher", "NXP Semiconductors"],
                [regKey, "HelpLink", "https://community.nxp.com"],
                [regKey, "InstallLocation", StringUtils.Replace(this.installerPath + "/", "/", "\\")],
                [regKey, "ModifyPath", StringUtils.Replace(this.launcherPath + "/", "/", "\\")],
                [regKey, "UninstallPath", StringUtils.Replace(this.installerPath + "/", "/", "\\")],
                [regKey, "UninstallString", "\"" + StringUtils.Replace(this.launcherPath, "/", "\\") + "\""]
            ], ($status : boolean) : void => {
                if ($status) {
                    this.callback(true);
                } else {
                    this.callback($status, "Unable to register application uninstaller.");
                }
            });
        }

        private getDesktopShortcutPath($callback : ($value : string) => void) : void {
            this.utils.getDesktopPath(true, ($path : string) : void => {
                $callback($path + "/NXP GPU SDK.lnk");
            });
        }

        private getStartMenuShortcutPath($callback : ($value : string) => void) : void {
            this.utils.getStartMenuPath(true, ($path : string) : void => {
                $callback($path + "/" + this.relativePath + "/" + this.appName + ".lnk");
            });
        }

        private getInstallerShortcutPath($callback : ($value : string) => void) : void {
            this.utils.getSystemDrive(($drive : string | boolean) : void => {
                if ($drive !== false) {
                    if (!StringUtils.StartsWith(this.installPath, <string>$drive)) {
                        this.installPath = $drive + "/" + this.installPath;
                    }
                    $callback(this.installPath + "/" + this.appName + ".lnk");
                } else {
                    ExceptionsManager.Throw(this.getClassName(), "Unable to get System Drive.");
                }
            });
        }
    }

    export interface IInstallUnistallPromise {
        Then($callback : ($success : boolean, $message? : string) => void) : void;
    }

    export interface ISelfinstallPromise {
        OnValidate($callback : ($success : boolean) => void) : ISelfinstallPromise;

        OnChange($callback : ($args : ProgressEventArgs) => void) : IInstallUnistallPromise;

        Then($callback : ($success : boolean, $message? : string) => void) : void;
    }

    export interface IInstalledPromise {
        Then($callback : ($success : boolean) => void) : void;
    }

    export interface IBSPVersionPromise {
        Then($callback : ($version : string) => void) : void;
    }
}
