/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Freescale {
    "use strict";

    export const GpuSdk = {
        Installer   : {
            Gui     : Io.GpuSdk.Installer.Gui,
            Services: Io.GpuSdk.Installer.Services
        },
        UserControls: Io.VisionSDK.UserControls
    };
}
