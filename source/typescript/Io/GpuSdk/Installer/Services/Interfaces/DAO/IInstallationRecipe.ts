/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.GpuSdk.Installer.Services.Interfaces.DAO {
    "use strict";
    import IInstallationEnvironment = Com.Wui.Framework.Services.Interfaces.DAO.IInstallationEnvironment;
    import IInstallationUtilValuePromise = Com.Wui.Framework.Services.Interfaces.DAO.IInstallationUtilValuePromise;
    import IInstallationUtilAcknowledgePromise = Com.Wui.Framework.Services.Interfaces.DAO.IInstallationUtilAcknowledgePromise;
    import LogInPanelType = Io.GpuSdk.Installer.Gui.BaseInterface.Enums.Panels.LogInPanelType;
    import IInstallationUtilStatusOrValuePromise = Com.Wui.Framework.Services.Interfaces.DAO.IInstallationUtilStatusOrValuePromise;
    import IInstallationPackage = Com.Wui.Framework.Services.Interfaces.DAO.IInstallationPackage;

    export abstract class IInstallationRecipe extends IInstallationEnvironment {
        public casSession : string;
        public onLoginRequest : ($type : LogInPanelType, $where? : string) => void;
        public getNXPwebCache : ($colCode : string, $done : IInstallationUtilValuePromise) => void;
        public uninstallRegisterExists : ($regKey : string, $version : string, $done : IInstallationUtilAcknowledgePromise) => void;
        public doubleUnpack : ($firstPackage : string, $secondPackage : string, $done : IInstallationUtilValuePromise) => void;
        public getInstallFolder : ($done : ($value : string | boolean) => void) => void;
        public installVTK : ($source : string, $package : IInstallationPackage, $done : IInstallationUtilStatusOrValuePromise) => void;
        public validateOpenCV : ($version : string, $done : IInstallationUtilStatusOrValuePromise) => void;
        public installOpenCV : ($source : string, $version : string, $done : IInstallationUtilStatusOrValuePromise) => void;
    }
}
