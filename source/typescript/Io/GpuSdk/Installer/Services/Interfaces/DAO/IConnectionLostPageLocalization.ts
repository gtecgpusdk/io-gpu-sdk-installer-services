/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.GpuSdk.Installer.Services.Interfaces.DAO {
    "use strict";
    import IBasePageLocalization = Com.Wui.Framework.Services.Interfaces.DAO.IBasePageLocalization;

    export abstract class IConnectionLostPageLocalization extends IBasePageLocalization {
        public header : string;
        public status : string;
        public exitButton : string;
    }
}
