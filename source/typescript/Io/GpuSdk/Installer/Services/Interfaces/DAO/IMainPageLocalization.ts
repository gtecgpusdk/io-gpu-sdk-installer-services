/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.GpuSdk.Installer.Services.Interfaces.DAO {
    "use strict";
    import IBasePageLocalization = Com.Wui.Framework.Services.Interfaces.DAO.IBasePageLocalization;

    export abstract class IMainPageButtonsLocalization {
        public next : string;
        public back : string;
        public cancel : string;
        public finish : string;
    }

    export abstract class IInitialScreenTaskLocalization {
        public install : string;
        public cancel : string;
        public uninstall : string;
        public repair : string;
        public switchBsp : string;
    }

    export abstract class IInitialScreenLocalization {
        public buttons : IInitialScreenTaskLocalization;
        public infoBox : IInitialScreenTaskLocalization;
    }

    export abstract class ILicenceAgreementLocalization {
        public title : string;
        public licence : string;
        public scrGlue : string;
        public checkBox : string;
    }

    export abstract class IProductDescriptionLocalization {
        public title : string;
        public description : string;
        public items : string[];
    }

    export abstract class IProductScreenLocalization {
        public title : string;
        public supported : IProductDescriptionLocalization;
        public legacy : IProductDescriptionLocalization;
        public internal : IProductDescriptionLocalization;
    }

    export abstract class IProductSummaryItemInfoBox {
        public optional : string;
        public upToDate : string;
        public size : string;
        public version : string;
        public yes : string;
        public no : string;
    }

    export abstract class IProductSummaryNotifications {
        /* tslint:disable: variable-name */
        public not_enough_disk_space : string;
        /* tslint:enable */
    }

    export abstract class IProductSummaryLocalization {
        public title : string;
        public estimateSize : string;
        public selectAll : string;
        public itemInfoBox : IProductSummaryItemInfoBox;
        public notifications : IProductSummaryNotifications;
    }

    export abstract class IInstallationProcessNotifications {
        /* tslint:disable: variable-name */
        public initialization : string;
        public waiting_for_install : string;
        public downloading : string;
        public extracting : string;
        public validating : string;
        /* tslint:enable */
    }

    export abstract class IInstallationProcessLocalization {
        public title : string;
        public progressMessage : string;
        public status : string;
        public notifications : IInstallationProcessNotifications;
    }

    export abstract class IFinalScreenLocalization {
        public title : string;
        public status : string;
        public desktopShortcut : string;
        public startMenuShortcut : string;
        public openUserGuideShortcut : string;
    }

    export abstract class IChangeDirectoryLocalization {
        public title : string;
        public defaultPath : string;
    }

    export abstract class ILogInPolicyLinkLocalization {
        public text : string;
        public url : string;
    }

    export abstract class ILogInNotifications {
        /* tslint:disable: variable-name */
        public processing_login : string;
        public bad_user_name_or_password : string;
        public terms_of_use_not_accepted : string;
        public unable_to_log_into_the_nxp_web_due_to_system_outage : string;
        /* tslint:enable */
    }

    export abstract class ILogInLocalization {
        public title : string;
        public username : string;
        public password : string;
        public tooltip : string;
        public infoBox : string;
        public policyLink : ILogInPolicyLinkLocalization;
        public notifications : ILogInNotifications;
    }

    export abstract class ISelfupdateLocalization {
        public title : string;
    }

    export abstract class IMainPageLocalization extends IBasePageLocalization {
        public buttons : IMainPageButtonsLocalization;
        public initialScreen : IInitialScreenLocalization;
        public licenceAgreement : ILicenceAgreementLocalization;
        public productScreen : IProductScreenLocalization;
        public productSummary : IProductSummaryLocalization;
        public installationProcess : IInstallationProcessLocalization;
        public uninstallProcess : IInstallationProcessLocalization;
        public finalScreen : IFinalScreenLocalization;
        public changeDirectory : IChangeDirectoryLocalization;
        public login : ILogInLocalization;
        public selfupdate : ISelfupdateLocalization;
    }
}
