/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.GpuSdk.Installer.Services.Interfaces.DAO {
    "use strict";
    import IBasePageLocalization = Com.Wui.Framework.Services.Interfaces.DAO.IBasePageLocalization;

    export abstract class IErrorPageNotificationsLocalization {
        /* tslint:disable: variable-name */
        public collecting_crash_report_data : string;
        public sending_crash_report : string;
        public report_was_sent_successfully : string;
        public we_are_sorry__installation_was_not_finished_successfully_and_we_were_unable_to_send_a_crash_report : string;
        /* tslint:enable */
    }

    export abstract class IErrorPageSupportLinkLocalization {
        public text : string;
        public url : string;
    }

    export abstract class IErrorPageSupportLocalization {
        public text : string;
        public link : IErrorPageSupportLinkLocalization;
    }

    export abstract class IErrorPageButtonsLocalization {
        public log : string;
        public exit : string;
    }

    export abstract class IErrorPageLocalization extends IBasePageLocalization {
        public header : string;
        public support : IErrorPageSupportLocalization;
        public manualReport : IErrorPageSupportLocalization;
        public notifications : IErrorPageNotificationsLocalization;
        public buttons : IErrorPageButtonsLocalization;
    }
}
