/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.GpuSdk.Installer.Services.Interfaces {
    "use strict";

    export abstract class IProject extends Com.Wui.Framework.Commons.Interfaces.IProject {
        public target : IProjectTarget;
    }

    export abstract class IProjectTarget extends Com.Wui.Framework.Commons.Interfaces.IProjectTarget {
        public hubLocation : string;
    }
}
