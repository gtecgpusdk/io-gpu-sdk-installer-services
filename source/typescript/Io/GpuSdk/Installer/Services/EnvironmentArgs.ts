/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.GpuSdk.Installer.Services {
    "use strict";
    import IProject = Io.GpuSdk.Installer.Services.Interfaces.IProject;

    export class EnvironmentArgs extends Com.Wui.Framework.Commons.EnvironmentArgs {
        public getProjectConfig() : IProject {
            return <IProject>super.getProjectConfig();
        }
    }
}
