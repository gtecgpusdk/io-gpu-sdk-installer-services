/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.GpuSdk.Installer.Services {
    "use strict";
    import HttpManager = Com.Wui.Framework.Services.HttpProcessor.HttpManager;
    import HttpResolver = Io.GpuSdk.Installer.Services.HttpProcessor.HttpResolver;
    import EnvironmentArgs = Com.Wui.Framework.Commons.EnvironmentArgs;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    /**
     * Loader class provides handling of application content singleton.
     */
    export class Loader extends Com.Wui.Framework.Services.Loader {

        private appLog : string = "";

        public static getInstance() : Loader {
            return <Loader>super.getInstance();
        }

        constructor() {
            super();
            this.appLog = "";
            LogIt.setOnPrint(($message : string) : void => {
                this.appLog += $message;
            });
        }

        public getHttpResolver() : HttpResolver {
            return <HttpResolver>super.getHttpResolver();
        }

        public getHttpManager() : HttpManager {
            return <HttpManager>super.getHttpManager();
        }

        public ApplicationLog($value? : string) : string {
            if (ObjectValidator.IsSet($value)) {
                this.appLog = $value;
            }
            return this.appLog;
        }

        public getEnvironmentArgs() : EnvironmentArgs {
            return <EnvironmentArgs>super.getEnvironmentArgs();
        }

        protected initResolver() : HttpResolver {
            return new HttpResolver(this.getEnvironmentArgs().getProjectName());
        }

        protected initEnvironment() : EnvironmentArgs {
            return new EnvironmentArgs();
        }
    }
}
