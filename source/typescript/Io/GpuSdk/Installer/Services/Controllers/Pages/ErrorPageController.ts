/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.GpuSdk.Installer.Services.Controllers.Pages {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ErrorPageViewer = Io.GpuSdk.Installer.Gui.BaseInterface.Viewers.Pages.ErrorPageViewer;
    import ErrorPageDAO = Io.GpuSdk.Installer.Services.DAO.Pages.ErrorPageDAO;
    import IErrorPageLocalization = Io.GpuSdk.Installer.Services.Interfaces.DAO.IErrorPageLocalization;
    import ErrorPageViewerArgs = Io.GpuSdk.Installer.Gui.BaseInterface.ViewersArgs.Pages.ErrorPageViewerArgs;
    import LanguageType = Com.Wui.Framework.Commons.Enums.LanguageType;
    import IconType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.IconType;
    import ImageButtonType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.ImageButtonType;
    import ErrorPage = Io.GpuSdk.Installer.Gui.BaseInterface.Pages.ErrorPage;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import ErrorPageType = Io.GpuSdk.Installer.Gui.BaseInterface.Enums.Pages.ErrorPageType;
    import Icon = Io.VisionSDK.UserControls.BaseInterface.UserControls.Icon;
    import Label = Io.VisionSDK.UserControls.BaseInterface.UserControls.Label;
    import ImageButton = Io.VisionSDK.UserControls.BaseInterface.UserControls.ImageButton;
    import ResizeBar = Com.Wui.Framework.UserControls.BaseInterface.Components.ResizeBar;
    import ReportServiceConnector = Com.Wui.Framework.Services.Connectors.ReportServiceConnector;
    import IReportProtocol = Com.Wui.Framework.Services.Connectors.IReportProtocol;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ElementEventsManager = Com.Wui.Framework.Gui.Events.ElementEventsManager;
    import ObjectEncoder = Com.Wui.Framework.Commons.Utils.ObjectEncoder;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import FileSystemHandlerConnector = Com.Wui.Framework.Services.Connectors.FileSystemHandlerConnector;
    import IEventsHandler = Com.Wui.Framework.Commons.Interfaces.IEventsHandler;
    import EnvironmentArgs = Com.Wui.Framework.Commons.EnvironmentArgs;

    export class ErrorPageController extends Com.Wui.Framework.Services.HttpProcessor.Resolvers.ExceptionPageController {
        private readonly manualReportEnabled : boolean;

        constructor() {
            super();
            this.setModelClassName(ErrorPageViewer);
            this.setDao(new ErrorPageDAO());

            this.setPageTitle("GPU SDK Installer - Error report");
            this.setPageIconSource("resource/graphics/GpuSdk.ico");
            this.loaderIcon.IconType(IconType.SPINNER_WHITE_BIG);
            this.appIcon.Visible(false);
            this.appTitle.Visible(false);
            this.minimizeButton.GuiType(ImageButtonType.APP_MINIMIZE);
            this.maximizeButton.Visible(false);
            this.closeButton.GuiType(ImageButtonType.APP_CLOSE);
            this.manualReportEnabled = false;
        }

        public getPageConfiguration() : IErrorPageLocalization {
            return <IErrorPageLocalization>super.getPageConfiguration();
        }

        public getModelArgs($language? : LanguageType, $asyncHandler? : ($args : ErrorPageViewerArgs) => void) : ErrorPageViewerArgs {
            return <ErrorPageViewerArgs>super.getModelArgs($language, $asyncHandler);
        }

        public getModel() : ErrorPageViewer {
            return <ErrorPageViewer>super.getModel();
        }

        protected getDao() : ErrorPageDAO {
            return <ErrorPageDAO>super.getDao();
        }

        protected resolver() : void {
            this.getEventsManager().setEvent(this.getClassName(), EventType.ON_SUCCESS, () : void => {
                const model : ErrorPageViewer = this.getModel();
                const instance : ErrorPage = this.getModel().getInstance();
                const args : ErrorPageViewerArgs = this.getModelArgs();
                const dao : ErrorPageDAO = this.getDao();
                let layer : ErrorPageType;

                const selectLayer : any = ($layer : ErrorPageType) : void => {
                    layer = $layer;
                    dao.SelectNotificationFor($layer);
                    model.ViewerArgs(args);
                    model.setLayer($layer);
                };

                let reportTimeout : any;
                const stopTimeout : any = () : void => {
                    if (!ObjectValidator.IsEmptyOrNull(reportTimeout)) {
                        clearTimeout(reportTimeout);
                    }
                };
                const resetTimeout : any = () : void => {
                    stopTimeout();
                    reportTimeout = setTimeout(() : void => {
                        selectLayer(failureLayer);
                    }, 10000);
                };

                selectLayer(ErrorPageType.INITIAL_SCREEN);
                resetTimeout();

                const environment : EnvironmentArgs = this.getEnvironmentArgs();
                const connector : ReportServiceConnector = new ReportServiceConnector();
                const data : IReportProtocol = {
                    appId      : StringUtils.getSha1(environment.getAppName() + environment.getProjectVersion()),
                    appName    : environment.getAppName(),
                    appVersion : environment.getProjectVersion(),
                    log        : Loader.getInstance().ApplicationLog(),
                    printScreen: "",
                    timeStamp  : new Date().getTime(),
                    trace      : this.getTrace()
                };

                const events : ElementEventsManager = new ElementEventsManager(instance, instance.Id() + "_Support");
                events.setOnClick(() : void => {
                    if (layer === ErrorPageType.MANUAL_REPORT) {
                        const bodyMaxSize : number = 256 * 4;
                        let body : string =
                            "Application: " + environment.getAppName() + " " + environment.getProjectVersion() +
                            StringUtils.NewLine(false) + StringUtils.NewLine(false) +
                            "Details:" + StringUtils.NewLine(false);

                        const trimBody : ($message : string) => string = ($message : string) : string => {
                            if (StringUtils.Length($message) > bodyMaxSize - 4) {
                                $message = StringUtils.Substring($message, 0, bodyMaxSize - 4) + " ...";
                            }
                            return $message;
                        };
                        if (!ObjectValidator.IsEmptyOrNull(data.trace)) {
                            body += data.trace;
                        } else {
                            body += data.log;
                        }

                        const link : HTMLAnchorElement = document.createElement("a");
                        link.href = StringUtils.Format("mailto:{0}?subject={1}&body={2}", dao.getPageConfiguration().manualReport.link.url,
                            ObjectEncoder.Url("Crash report from user"), trimBody(ObjectEncoder.Url(body)));
                        link.click();
                    } else {
                        this.getHttpManager().ReloadTo(dao.getPageConfiguration().support.link.url, true);
                    }
                });
                instance.supportLabel.getEvents().setOnComplete(() : void => {
                    events.Subscribe();
                });

                if (this.getRequest().IsWuiJre()) {
                    instance.exitButton.getEvents().setOnClick(() : void => {
                        window.close();
                    });
                }

                const failureLayer : ErrorPageType = this.manualReportEnabled ? ErrorPageType.MANUAL_REPORT : ErrorPageType.REPORT_FAIL;
                connector.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
                    try {
                        LogIt.Error($eventArgs.ToString("", false));
                    } catch (ex) {
                        LogIt.Debug("Unable to consume Report service connector error.", ex);
                    }
                    selectLayer(failureLayer);
                });
                connector.getEvents().OnTimeout(() : void => {
                    LogIt.Error("Report service connector is unreachable.");
                    selectLayer(failureLayer);
                });

                const sendReport : any = () : void => {
                    this.getHttpManager().IsOnline(($status : boolean) : void => {
                        if ($status) {
                            connector
                                .CreateReport(data)
                                .Then(($success : boolean) : void => {
                                    stopTimeout();
                                    if ($success) {
                                        Loader.getInstance().ApplicationLog("");
                                        selectLayer(ErrorPageType.REPORT_OK);
                                    } else {
                                        selectLayer(failureLayer);
                                    }
                                });
                        } else {
                            stopTimeout();
                            selectLayer(failureLayer);
                        }
                    });
                };

                const fileSystem : FileSystemHandlerConnector = new FileSystemHandlerConnector();
                const errorEventHandler : IEventsHandler = () : void => {
                    if (this.getRequest().IsWuiJre()) {
                        sendReport();
                    }
                };
                fileSystem.getEvents().OnError(errorEventHandler);
                fileSystem.getEvents().OnTimeout(errorEventHandler);

                const today : Date = new Date();
                const year : string = today.getFullYear() + "";
                const monthNumber : number = today.getMonth() + 1;
                const month : string = monthNumber < 10 ? "0" + monthNumber : monthNumber + "";
                const day : string = today.getDate() < 10 ? "0" + today.getDate() : today.getDate() + "";
                const logFile : string = "log/" + year + "/" + month + "/" + day + "_" + month + "_" + year + ".txt";
                fileSystem.getWuiHostLocation().Then(($path : string) : void => {
                    fileSystem.Read($path + "/" + logFile).Then(($data : string) : void => {
                        const currentLength : number = StringUtils.Length($data);
                        if (currentLength > 0) {
                            const maxLength : number = 1024 * 10;
                            if (currentLength > maxLength) {
                                $data = StringUtils.Substring($data, currentLength - maxLength);
                            }
                            data.log += "\r\n-------------------- WUI Connector --------------------\r\n\r\n" + $data;
                        }
                        resetTimeout();
                        sendReport();
                    });
                });
            });
            super.resolver();
        }

        /**
         * @return {IIcon} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.UserControls.IIcon
         */
        protected getLoaderIconClass() : any {
            return Icon;
        }

        /**
         * @return {ILabel} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.UserControls.ILabel
         */
        protected getLoaderTextClass() : any {
            return Label;
        }

        /**
         * @return {ImageButton} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.UserControls.ImageButton
         */
        protected getAppImageButtonClass() : any {
            return ImageButton;
        }

        /**
         * @return {IIcon} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.UserControls.IIcon
         */
        protected getAppIconClass() : any {
            return Icon;
        }

        /**
         * @return {ILabel} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.UserControls.ILabel
         */
        protected getAppTitleTextClass() : any {
            return Label;
        }

        /**
         * @return {IResizeBar} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IResizeBar
         */
        protected getAppResizeBarClass() : any {
            return ResizeBar;
        }
    }
}
