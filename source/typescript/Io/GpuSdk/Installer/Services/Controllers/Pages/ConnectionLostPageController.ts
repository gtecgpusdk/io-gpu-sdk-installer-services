/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.GpuSdk.Installer.Services.Controllers.Pages {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LanguageType = Com.Wui.Framework.Commons.Enums.LanguageType;
    import IconType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.IconType;
    import ImageButtonType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.ImageButtonType;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ConnectionLostPageViewer = Io.GpuSdk.Installer.Gui.BaseInterface.Viewers.Pages.ConnectionLostPageViewer;
    import IConnectionLostPageLocalization = Io.GpuSdk.Installer.Services.Interfaces.DAO.IConnectionLostPageLocalization;
    import ConnectionLostPageViewerArgs = Io.GpuSdk.Installer.Gui.BaseInterface.ViewersArgs.Pages.ConnectionLostPageViewerArgs;
    import ConnectionLostPageDAO = Io.GpuSdk.Installer.Services.DAO.Pages.ConnectionLostPageDAO;
    import ConnectionLostPage = Io.GpuSdk.Installer.Gui.BaseInterface.Pages.ConnectionLostPage;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;

    export class ConnectionLostPageController extends Io.GpuSdk.Installer.Services.HttpProcessor.Resolvers.BasePageController {

        constructor() {
            super();
            this.setModelClassName(ConnectionLostPageViewer);
            this.setDao(new ConnectionLostPageDAO());

            this.setPageTitle("GPU SDK Installer - Connection lost");
            this.setPageIconSource("resource/graphics/GpuSdk.ico");
            this.loaderIcon.IconType(IconType.SPINNER_WHITE_BIG);
            this.appIcon.Visible(false);
            this.appTitle.Visible(false);
            this.minimizeButton.GuiType(ImageButtonType.APP_MINIMIZE);
            this.maximizeButton.Visible(false);
            this.closeButton.GuiType(ImageButtonType.APP_CLOSE);
        }

        public getPageConfiguration() : IConnectionLostPageLocalization {
            return <IConnectionLostPageLocalization>super.getPageConfiguration();
        }

        public getModelArgs($language? : LanguageType,
                            $asyncHandler? : ($args : ConnectionLostPageViewerArgs) => void) : ConnectionLostPageViewerArgs {
            return <ConnectionLostPageViewerArgs>super.getModelArgs($language, $asyncHandler);
        }

        public getModel() : ConnectionLostPageViewer {
            return <ConnectionLostPageViewer>super.getModel();
        }

        protected getDao() : ConnectionLostPageDAO {
            return <ConnectionLostPageDAO>super.getDao();
        }

        protected resolver() : void {
            this.getEventsManager().setEvent(this.getClassName(), EventType.ON_SUCCESS, () : void => {
                const instance : ConnectionLostPage = this.getModel().getInstance();
                const args : ConnectionLostPageViewerArgs = this.getModelArgs();

                instance.exitButton.getEvents().setOnClick(() : void => {
                    window.close();
                });

                let countDown : number = 24;
                instance.statusLabel.Text(StringUtils.Format(args.StatusText(), countDown));
                const counter : any = setInterval(() : void => {
                    try {
                        countDown--;
                        if (countDown <= 0) {
                            clearInterval(counter);
                            this.getHttpManager().ReloadTo("/", null, true);
                        } else {
                            instance.statusLabel.Text(StringUtils.Format(args.StatusText(), countDown));
                        }
                    } catch (ex) {
                        ExceptionsManager.HandleException(ex);
                    }
                }, 1000);
            });
            super.resolver();
        }
    }
}
