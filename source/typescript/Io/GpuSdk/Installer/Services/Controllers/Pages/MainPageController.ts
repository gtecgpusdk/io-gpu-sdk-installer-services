/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.GpuSdk.Installer.Services.Controllers.Pages {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import MainPageViewer = Io.GpuSdk.Installer.Gui.BaseInterface.Viewers.Pages.MainPageViewer;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import MainPage = Io.GpuSdk.Installer.Gui.BaseInterface.Pages.MainPage;
    import MainPageDAO = Io.GpuSdk.Installer.Services.DAO.Pages.MainPageDAO;
    import IMainPageLocalization = Io.GpuSdk.Installer.Services.Interfaces.DAO.IMainPageLocalization;
    import MainPageViewerArgs = Io.GpuSdk.Installer.Gui.BaseInterface.ViewersArgs.Pages.MainPageViewerArgs;
    import LanguageType = Com.Wui.Framework.Commons.Enums.LanguageType;
    import InstallationContentPanelType = Io.GpuSdk.Installer.Gui.BaseInterface.Enums.Panels.InstallationContentPanelType;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import MainPageType = Io.GpuSdk.Installer.Gui.BaseInterface.Enums.Pages.MainPageType;
    import InstallationContentPanel = Io.GpuSdk.Installer.Gui.BaseInterface.Panels.InstallationContentPanel;
    import InstallationContentPanelViewer = Io.GpuSdk.Installer.Gui.BaseInterface.Viewers.Panels.InstallationContentPanelViewer;
    import InstallationProcessPanel = Io.GpuSdk.Installer.Gui.BaseInterface.Panels.InstallationProcessPanel;
    import IEventsHandler = Com.Wui.Framework.Gui.Interfaces.IEventsHandler;
    import ProductScreenPanelType = Io.GpuSdk.Installer.Gui.BaseInterface.Enums.Panels.ProductScreenPanelType;
    import InitialScreenPanelType = Io.GpuSdk.Installer.Gui.BaseInterface.Enums.Panels.InitialScreenPanelType;
    import ProductSummaryPanelType = Io.GpuSdk.Installer.Gui.BaseInterface.Enums.Panels.ProductSummaryPanelType;
    import InstallationRecipeDAO = Com.Wui.Framework.Services.DAO.InstallationRecipeDAO;
    import InstallationProgressEventArgs = Com.Wui.Framework.Services.Events.Args.InstallationProgressEventArgs;
    import InstallationProtocolType = Com.Wui.Framework.Services.Enums.InstallationProtocolType;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import IInstallationPackage = Com.Wui.Framework.Services.Interfaces.DAO.IInstallationPackage;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ReportServiceConnector = Com.Wui.Framework.Services.Connectors.ReportServiceConnector;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import BSPSelector = Io.VisionSDK.UserControls.BaseInterface.UserControls.BSPSelector;
    import ProgressEventArgs = Com.Wui.Framework.Commons.Events.Args.ProgressEventArgs;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import IInstallationRecipe = Io.GpuSdk.Installer.Services.Interfaces.DAO.IInstallationRecipe;
    import LogInPanelType = Io.GpuSdk.Installer.Gui.BaseInterface.Enums.Panels.LogInPanelType;
    import InstallationItem = Io.VisionSDK.UserControls.BaseInterface.UserControls.InstallationItem;
    import InstallationProgressCode = Com.Wui.Framework.Services.Enums.InstallationProgressCode;
    import InstallerAppManager = Io.GpuSdk.Installer.Services.Utils.InstallerAppManager;
    import ProgressBarType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.ProgressBarType;
    import WindowHandlerConnector = Com.Wui.Framework.Services.Connectors.WindowHandlerConnector;
    import WindowHandlerOpenOptions = Com.Wui.Framework.Services.Connectors.WindowHandlerOpenOptions;
    import WindowHandlerScriptExecuteOptions = Com.Wui.Framework.Services.Connectors.WindowHandlerScriptExecuteOptions;
    import IWindowHandlerCookie = Com.Wui.Framework.Services.Connectors.IWindowHandlerCookie;
    import KeyEventArgs = Com.Wui.Framework.Gui.Events.Args.KeyEventArgs;
    import KeyMap = Com.Wui.Framework.Gui.Enums.KeyMap;
    import TaskBarProgressState = Com.Wui.Framework.Services.Enums.TaskBarProgressState;
    import Button = Io.VisionSDK.UserControls.BaseInterface.UserControls.Button;
    import ElementEventsManager = Com.Wui.Framework.Gui.Events.ElementEventsManager;
    import IProductSummaryItemInfoBox = Io.GpuSdk.Installer.Services.Interfaces.DAO.IProductSummaryItemInfoBox;
    import IInstallationProcessNotifications = Io.GpuSdk.Installer.Services.Interfaces.DAO.IInstallationProcessNotifications;
    import ProductScreenPanelViewerArgs = Io.GpuSdk.Installer.Gui.BaseInterface.ViewersArgs.Panels.ProductScreenPanelViewerArgs;
    import ObjectDecoder = Com.Wui.Framework.Commons.Utils.ObjectDecoder;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;

    export class MainPageController extends Io.GpuSdk.Installer.Services.HttpProcessor.Resolvers.BasePageController {
        private readonly installDao : InstallationRecipeDAO;
        private isInternal : boolean;
        private isVisionSDK : boolean;
        private appManager : InstallerAppManager;
        private hubLocation : string;

        constructor() {
            super();
            this.setModelClassName(MainPageViewer);
            this.setDao(new MainPageDAO());
            this.installDao = new InstallationRecipeDAO();
            this.isInternal = !this.getEnvironmentArgs().IsProductionMode();
            this.isVisionSDK = false;
            this.hubLocation = this.getEnvironmentArgs().getProjectConfig().target.hubLocation;
            LogIt.Debug("Using Hub: {0}", this.hubLocation);
        }

        public getPageConfiguration() : IMainPageLocalization {
            return <IMainPageLocalization>super.getPageConfiguration();
        }

        public getModelArgs($language? : LanguageType, $asyncHandler? : ($args : MainPageViewerArgs) => void) : MainPageViewerArgs {
            return <MainPageViewerArgs>super.getModelArgs($language, $asyncHandler);
        }

        public getModel() : MainPageViewer {
            return <MainPageViewer>super.getModel();
        }

        protected getDao() : MainPageDAO {
            return <MainPageDAO>super.getDao();
        }

        protected loadResources() : void {
            this.installDao.Load(LanguageType.EN, () : void => {
                super.loadResources();
            });
        }

        protected argsHandler($GET : ArrayList<string>) : void {
            super.argsHandler($GET);
            if ($GET.KeyExists("type")) {
                switch ($GET.getItem("type")) {
                case "Internal":
                    this.isInternal = true;
                    break;
                case "VisionSDK":
                    this.isVisionSDK = true;
                    break;
                default:
                    break;
                }
            } else {
                if ($GET.Contains("isInternal")) {
                    this.isInternal = true;
                } else if ($GET.Contains("isPublic")) {
                    this.isInternal = false;
                }
                if ($GET.Contains("isVision")) {
                    this.isVisionSDK = true;
                }
            }
        }

        protected beforeLoad($instance : MainPage, $args : MainPageViewerArgs, $dao : MainPageDAO) : void {
            let isUninstall : boolean = false;
            let isRepair : boolean = false;
            let isLoggedIn : boolean = true;
            let loginRequestUrl : string;
            let loginPageOpened : boolean = false;
            const visionSDKProfileName : string = "VisionSDKStudio";

            const model : MainPageViewer = this.getModel();
            const installationContentInstance : InstallationContentPanel = $instance.installationContentPanel;
            const installationProcessInstance : InstallationProcessPanel = installationContentInstance.installationProcessPanel;
            const installationContentModel : InstallationContentPanelViewer =
                <InstallationContentPanelViewer>installationContentInstance.InstanceOwner();
            let fullChain : string[] = [];
            let requiredChain : InstallationItem[] = [];
            const appRootFolder : string = StringUtils.Remove(ObjectDecoder.Url(this.getRequest().getHostUrl()),
                "file:///", "file://", "/target/index.html");
            let selectedBsp : string = "";
            let diskLabel : string = "";
            let diskSpace : number = 0;
            let estimatedSize : number = 0;
            let currentProduct : string = "";
            let lastLength : number = 0;
            let currentLength : number = 0;
            const bandWidthBuffer : number[] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            let bandWidth : number = -1;
            let bandWidthEnabled : boolean = true;
            let estimatedTime : string;
            let startTime : number;
            const isDesktop : boolean = this.getRequest().IsWuiJre();
            let windowHandler : WindowHandlerConnector;
            if (isDesktop) {
                windowHandler = new WindowHandlerConnector();
            }

            if (this.getEnvironmentArgs().IsProductionMode()) {
                const report : ReportServiceConnector = new ReportServiceConnector();
                LogIt.setOnPrint(($message : string) : void => {
                    Loader.getInstance().ApplicationLog(Loader.getInstance().ApplicationLog() + $message);
                    report.LogIt($message);
                });
            }
            this.installDao.getEvents().setOnError(($eventArgs : ErrorEventArgs) : void => {
                this.installDao.Abort();
                ExceptionsManager.Throw(this.getClassName(), $eventArgs.Exception());
            });

            const updateSummaryPageInfo : any = () : void => {
                let selectAll : boolean = true;
                let index : number = 0;
                estimatedSize = InstallerAppManager.APP_SIZE;
                installationContentInstance.productSummaryPanel.items.getChildElements()
                    .foreach(($item : InstallationItem) : void => {
                        if ($item.IsTypeOf(InstallationItem)) {
                            const info : IInstallationPackage = this.installDao.getConfigurationInstance()
                                .packages[this.installDao.getChain()[index]];
                            if (!$item.Value()) {
                                selectAll = false;
                                if (requiredChain.indexOf($item) !== -1) {
                                    info.force = false;
                                }
                            } else {
                                if (!ObjectValidator.IsEmptyOrNull(info) && !ObjectValidator.IsEmptyOrNull(info.size)) {
                                    estimatedSize += info.size;
                                }
                                if (requiredChain.indexOf($item) === -1) {
                                    info.force = true;
                                }
                            }
                            index++;
                        }
                    });
                installationContentInstance.productSummaryPanel.selectAllCheckBox.Value(selectAll);
                installationContentInstance.productSummaryPanel.sizeLabel
                    .Text(StringUtils.Format($dao.getPageConfiguration().productSummary.estimateSize,
                        Convert.IntegerToSize(estimatedSize, 1),
                        Convert.IntegerToSize(diskSpace, 1),
                        diskLabel + "\\"));
                if (estimatedSize > diskSpace) {
                    installationContentInstance.productSummaryPanel.sizeLabel.StyleClassName("OutOfSpace");
                    installationContentInstance.nextButton.Enabled(false);
                    installationContentInstance.productSummaryPanel.Notification().Visible(true);
                } else {
                    installationContentInstance.productSummaryPanel.sizeLabel.StyleClassName("");
                    installationContentInstance.nextButton.Enabled(true);
                    installationContentInstance.productSummaryPanel.Notification().Visible(false);
                }
            };

            const getBandWidth : any = () : void => {
                setTimeout(() : void => {
                    let width : number = currentLength - lastLength;
                    if (width < 0) {
                        width = 0;
                    } else {
                        lastLength = currentLength;
                    }
                    bandWidthBuffer.push(width);
                    bandWidthBuffer.shift();
                    bandWidth = Convert.ToFixed(bandWidthBuffer.reduce(($a : number, $b : number) : number => {
                        return $a + $b;
                    }, 0) / bandWidthBuffer.length, 0) * 2;
                    currentLength = 0;
                    if (bandWidthEnabled) {
                        getBandWidth();
                    } else {
                        bandWidth = -1;
                    }
                }, 500);
            };

            const processNotifications : IInstallationProcessNotifications = !isUninstall ?
                $dao.getPageConfiguration().installationProcess.notifications :
                $dao.getPageConfiguration().uninstallProcess.notifications;
            let marqueTimer : number;
            this.installDao.getEvents().setOnChange(($eventArgs : InstallationProgressEventArgs) : void => {
                if (installationProcessInstance.Visible()) {
                    if ($eventArgs.ProgressCode() === InstallationProgressCode.CHAIN_START) {
                        currentProduct = $eventArgs.Message();
                        installationProcessInstance.statusLabel.Text(StringUtils.Format($dao.getPageConfiguration().installationProcess
                            .progressMessage, currentProduct, processNotifications.initialization));
                        installationProcessInstance.progressLabel.Visible(false);
                        if (isDesktop) {
                            windowHandler.setTaskBarProgressState(TaskBarProgressState.NORMAL);
                            windowHandler.setTaskBarProgressValue($eventArgs.CurrentValue(), $eventArgs.RangeEnd());
                        }
                    }

                    if ($eventArgs.ProgressCode() === InstallationProgressCode.STEP_START) {
                        switch ($eventArgs.Message()) {
                        case InstallationProtocolType.INSTALL:
                            installationProcessInstance.statusLabel.Text(StringUtils.Format($dao.getPageConfiguration().installationProcess
                                .progressMessage, currentProduct, processNotifications.waiting_for_install));
                            break;
                        case InstallationProtocolType.VALIDATE:
                            installationProcessInstance.statusLabel.Text(StringUtils.Format($dao.getPageConfiguration().installationProcess
                                .progressMessage, currentProduct, processNotifications.validating));
                            break;
                        default:
                            break;
                        }

                        LogIt.Info(installationProcessInstance.statusLabel.Text() + " step -> " + $eventArgs.Message());
                    }

                    if ($eventArgs.ProgressCode() === InstallationProgressCode.EXTRACT_START) {
                        bandWidthEnabled = false;
                        installationProcessInstance.statusLabel.Text(StringUtils.Format($dao.getPageConfiguration().installationProcess
                            .progressMessage, currentProduct, processNotifications.extracting));
                    }

                    if ($eventArgs.ProgressCode() === InstallationProgressCode.DOWNLOAD_START) {
                        installationProcessInstance.statusLabel.Text(StringUtils.Format($dao.getPageConfiguration().installationProcess
                            .progressMessage, currentProduct, processNotifications.downloading));

                        estimatedTime = "";
                        startTime = new Date().getTime();
                        bandWidthEnabled = true;
                        getBandWidth();
                        installationProcessInstance.progressLabel.Visible(true);
                        installationProcessInstance.progressLabel.Text(" ");
                        $instance.getEvents().FireAsynchronousMethod(() : void => {
                            installationProcessInstance.progressBar.Value(0);
                            installationProcessInstance.progressBar.GuiType(ProgressBarType.GENERAL);
                            $instance.getEvents().FireAsynchronousMethod(() : void => {
                                installationProcessInstance.progressBar.Value(0);
                            }, false);
                        }, false);
                    } else if ($eventArgs.ProgressCode() === InstallationProgressCode.DOWNLOAD_CHANGE) {
                        if ($eventArgs.CurrentValue() !== -1) {
                            currentLength = $eventArgs.CurrentValue();
                            if (!bandWidthEnabled) {
                                bandWidthEnabled = true;
                                getBandWidth();
                            }

                            let forecast : number =
                                ($eventArgs.RangeEnd() - $eventArgs.CurrentValue()) /
                                ($eventArgs.CurrentValue() / ((new Date().getTime() - startTime) / 1000));
                            let bandWidthFormatted : string = "";
                            if (bandWidth > -1) {
                                bandWidthFormatted = ", " + Convert.IntegerToSize(bandWidth) + "/s";
                            }
                            if (forecast < 60) {
                                forecast = Convert.ToFixed(forecast, 0);
                                if (forecast > 0) {
                                    estimatedTime = " (" + Convert.ToFixed(forecast, 0) + " s" + bandWidthFormatted + ")";
                                } else {
                                    estimatedTime = "";
                                }
                            } else if (forecast >= 60 && forecast < 3600) {
                                estimatedTime =
                                    " (" + Convert.ToFixed(forecast / 60, 0) + " min" + bandWidthFormatted + ")";
                            } else if (forecast >= 3600) {
                                estimatedTime =
                                    " (" + Convert.ToFixed(forecast / 3600, 0) + " h" + bandWidthFormatted + ")";
                            }
                            installationProcessInstance.progressLabel.Text(
                                StringUtils.Format($dao.getPageConfiguration().installationProcess.status,
                                    Convert.IntegerToSize($eventArgs.CurrentValue(), 2),
                                    Convert.IntegerToSize($eventArgs.RangeEnd(), 2) + estimatedTime));
                            installationProcessInstance.progressLabel.Visible(true);
                            $instance.getEvents().FireAsynchronousMethod(() : void => {
                                installationProcessInstance.progressBar.Value(
                                    Convert.ToFixed(100 / $eventArgs.RangeEnd() * $eventArgs.CurrentValue(), 0));
                                installationProcessInstance.progressBar.GuiType(ProgressBarType.GENERAL);
                            }, false);
                        }
                    } else if ($eventArgs.ProgressCode() === InstallationProgressCode.STEP_START
                        && $eventArgs.Message() === InstallationProtocolType.INSTALL) {
                        bandWidthEnabled = false;
                        installationProcessInstance.progressLabel.Visible(false);
                        installationProcessInstance.progressBar.GuiType(ProgressBarType.MARQUEE);
                    }
                } else if (installationContentInstance.productSummaryPanel.Visible()) {
                    const validationBar : InstallationProcessPanel = installationContentInstance.productSummaryPanel.validationBar;
                    if ($eventArgs.ProgressCode() === InstallationProgressCode.CHAIN_START) {
                        const getText : any = ($index : number) : void => {
                            if ($index < $eventArgs.RangeEnd()) {
                                const info : IInstallationPackage = this.installDao.getPackage(
                                    this.installDao.getChain()[$index]);
                                let text : string = ObjectValidator.IsEmptyOrNull(info.description) ? info.name : info.description;
                                if (!ObjectValidator.IsEmptyOrNull(info.version)) {
                                    text += " (" + info.version + ")";
                                }
                                validationBar.progressLabel.Text(
                                    "Check of tool-chain item " + text + " " + ($index + 1) + "/" + $eventArgs.RangeEnd());
                            }
                        };
                        if (!ObjectValidator.IsEmptyOrNull(marqueTimer)) {
                            clearTimeout(marqueTimer);
                        }
                        validationBar.progressBar.RangeEnd($eventArgs.RangeEnd());
                        validationBar.progressBar.GuiType(ProgressBarType.GENERAL);
                        validationBar.progressBar.Value($eventArgs.CurrentValue());
                        validationBar.progressLabel.Visible(true);
                        getText($eventArgs.CurrentValue() - 1);
                        marqueTimer = this.getEventsManager().FireAsynchronousMethod(() : void => {
                            validationBar.progressBar.GuiType(ProgressBarType.MARQUEE);
                            getText($eventArgs.CurrentValue());
                        }, 500);
                    } else if ($eventArgs.ProgressCode() === InstallationProgressCode.VALIDATED) {
                        const info : IInstallationPackage = this.installDao.getPackage(
                            this.installDao.getChain()[$eventArgs.CurrentValue() - 1]);
                        let text : string = ObjectValidator.IsEmptyOrNull(info.description) ? info.name : info.description;
                        if (!ObjectValidator.IsEmptyOrNull(info.version)) {
                            text += " (" + info.version + ")";
                        }
                        if (!ObjectValidator.IsEmptyOrNull(info)) {
                            LogIt.Info(text + " -> " + $eventArgs.Status() + " : " + $eventArgs.Message());
                            let detail : string = "";
                            if (!ObjectValidator.IsEmptyOrNull(info.info)) {
                                detail += info.info + StringUtils.NewLine();
                            }
                            const infoLocalization : IProductSummaryItemInfoBox = $dao.getPageConfiguration().productSummary.itemInfoBox;
                            detail += StringUtils.Format(infoLocalization.optional,
                                info.optional ? infoLocalization.yes : infoLocalization.no) + StringUtils.NewLine();
                            detail += StringUtils.Format(infoLocalization.upToDate,
                                $eventArgs.Status() ? infoLocalization.yes : infoLocalization.no) + StringUtils.NewLine();
                            if (!ObjectValidator.IsEmptyOrNull(info.version)) {
                                detail += StringUtils.Format(infoLocalization.version, info.version) + StringUtils.NewLine();
                            }
                            if (!ObjectValidator.IsEmptyOrNull(info.size)) {
                                detail += StringUtils.Format(infoLocalization.size,
                                    Convert.IntegerToSize(info.size, 1)) + StringUtils.NewLine();
                            }
                            const item : InstallationItem = installationContentInstance.productSummaryPanel.Add(
                                text, detail,
                                false, !$eventArgs.Status(), info.optional);
                            item.getEvents().setOnComplete(updateSummaryPageInfo);
                            item.getEvents().setOnChange(updateSummaryPageInfo);
                            if (!$eventArgs.Status()) {
                                requiredChain.push(item);
                            }
                        }
                        if ($eventArgs.CurrentValue() === $eventArgs.RangeEnd()) {
                            installationContentInstance.backButton.Enabled(true);
                            installationContentInstance.productSummaryPanel.InstanceOwner().setLayer(ProductSummaryPanelType.ITEMS);
                            validationBar.progressBar.GuiType(ProgressBarType.GENERAL);
                            validationBar.progressBar.Value(0);
                            validationBar.progressLabel.Visible(false);
                            if (!ObjectValidator.IsEmptyOrNull(marqueTimer)) {
                                clearTimeout(marqueTimer);
                            }
                        }
                    }
                } else if (!isUninstall) {
                    if ($eventArgs.ProgressCode() === InstallationProgressCode.VALIDATED) {
                        LogIt.Info($eventArgs.Message() + " " + $eventArgs.CurrentValue() + "/" + $eventArgs.RangeEnd());
                        if (!$eventArgs.Status()) {
                            this.installDao.Abort();
                            if (isDesktop) {
                                windowHandler.setTaskBarProgressState(TaskBarProgressState.ERROR);
                                windowHandler.setTaskBarProgressValue($eventArgs.RangeEnd(), $eventArgs.RangeEnd());
                            }
                            ExceptionsManager.Throw(this.getClassName(), "Not all off packages have been installed correctly.");
                        }
                    }
                }
            });

            const selectInstallationContentLayer : ($type : InstallationContentPanelType) => void =
                ($type : InstallationContentPanelType) : void => {
                    $dao.SelectInstallationContentLayer($type);
                    model.ViewerArgs($args);
                    installationContentModel.setLayer($type);
                };

            this.installDao.getEvents().setOnComplete(() : void => {
                if (installationProcessInstance.Visible()) {
                    installationProcessInstance.progressBar.Value(100);
                    $instance.getEvents().FireAsynchronousMethod(() : void => {
                        if (!isUninstall) {
                            if (!installationContentInstance.finalScreenPanel.Visible()) {
                                this.installDao.RunInstallationChain(InstallationProtocolType.VALIDATE);
                                if (isDesktop) {
                                    windowHandler.setTaskBarProgressState(TaskBarProgressState.NOPROGRESS);
                                }
                                selectInstallationContentLayer(InstallationContentPanelType.FINAL_SCREEN);
                            }
                        } else {
                            installationContentModel.setLayer(InstallationContentPanelType.CLOSING);
                            this.appManager.Uninstall().Then(($status : boolean, $message? : string) : void => {
                                if ($status) {
                                    if (isDesktop) {
                                        window.close();
                                    }
                                } else {
                                    ExceptionsManager.Throw(this.getClassName(), $message);
                                }
                            });
                        }
                    }, 350);
                }
            });

            const selectLogInContentLayer : ($type : LogInPanelType) => void = ($type : LogInPanelType) : void => {
                $dao.SelectLogInContentLayer($type);
                installationContentInstance.logInPanel.InstanceOwner().setLayer($type);
            };

            const loadProtocol : ($bspVersion : string, $callback : () => void) => void =
                ($bspVersion : string, $callback : () => void) : void => {
                    fullChain = [];
                    requiredChain = [];
                    selectedBsp = $bspVersion;
                    this.installDao.setConfigurationPath(this.hubLocation + "/Configuration/" +
                        this.getEnvironmentArgs().getProjectName() + "/" + $bspVersion);
                    this.installDao.Load(LanguageType.EN, $callback, true);
                };

            const selectProductSummaryLayer : any = () : void => {
                installationContentInstance.backButton.Enabled(false);
                installationContentInstance.nextButton.Enabled(false);
                installationContentInstance.productSummaryPanel.InstanceOwner().setLayer(ProductSummaryPanelType.LOADING);
                selectInstallationContentLayer(InstallationContentPanelType.PRODUCT_SUMMARY);
                installationContentInstance.contentTitle
                    .Text(StringUtils.Format(installationContentInstance.contentTitle.Text(), selectedBsp));
            };

            const showProductSummary : any = ($eventArgsOrBSPVersion : EventArgs | string) : void => {
                const onLoad : () => void = () : void => {
                    LogIt.Info("Processing installation recipe: " + this.installDao.getDaoDataSource());
                    selectProductSummaryLayer();
                    installationContentInstance.productSummaryPanel.sizeLabel
                        .Text(StringUtils.Format($dao.getPageConfiguration().productSummary.estimateSize,
                            Convert.IntegerToSize(estimatedSize, 1),
                            Convert.IntegerToSize(diskSpace, 1),
                            diskLabel + "\\"));
                    installationContentInstance.productSummaryPanel.selectAllCheckBox.Value(true);

                    installationContentInstance.productSummaryPanel.Clear();
                    this.installDao.getStaticConfiguration().getChain().forEach(($item : string) : void => {
                        fullChain.push($item);
                    });
                    (<IInstallationRecipe>this.installDao.getConfigurationInstance()).onLoginRequest =
                        ($type : LogInPanelType, $where : string) : void => {
                            this.installDao.Abort();
                            if (!installationContentInstance.logInPanel.Visible()) {
                                selectInstallationContentLayer(InstallationContentPanelType.LOGIN);
                            }
                            $instance.getEvents().FireAsynchronousMethod(() : void => {
                                if ($type === LogInPanelType.NOT_ACCEPTED) {
                                    selectLogInContentLayer(LogInPanelType.NOT_ACCEPTED);
                                    if (!loginPageOpened) {
                                        loginPageOpened = true;
                                        this.installDao.ConfigurationLibrary().terminal.Open("https://www.nxp.com/security/login");
                                    }
                                } else if ($type === LogInPanelType.SYSTEM_OUTAGE) {
                                    selectLogInContentLayer(LogInPanelType.SYSTEM_OUTAGE);
                                } else {
                                    isLoggedIn = false;
                                    selectLogInContentLayer(LogInPanelType.DEFAULT);
                                    loginRequestUrl = $where;
                                }
                            }, 150);
                        };
                    this.installDao.RunInstallationChain(InstallationProtocolType.INSTALL_STATUS);
                };

                if (isDesktop) {
                    windowHandler.setTaskBarProgressState(TaskBarProgressState.NOPROGRESS);
                }
                if (ObjectValidator.IsString($eventArgsOrBSPVersion)) {
                    selectedBsp = <string>$eventArgsOrBSPVersion;
                } else {
                    selectedBsp = (<BSPSelector>(<EventArgs>$eventArgsOrBSPVersion).Owner()).Value();
                }
                selectProductSummaryLayer();

                this.installDao.ConfigurationLibrary().terminal
                    .Spawn("wmic", [
                        "LogicalDisk", "Where", "DriveType=\"3\"", "Get", "DeviceID,FreeSpace"
                    ])
                    .Then(($exitCode : number, $stdout : string) : void => {
                        if (ObjectValidator.IsInteger($exitCode)) {
                            if ($exitCode === 0) {
                                this.installDao.getConfigurationInstance().utils.getSystemDrive(($value : string | boolean) : void => {
                                    if (!ObjectValidator.IsBoolean($value)) {
                                        diskLabel = <string>$value;
                                        $stdout = StringUtils.Substring($stdout, StringUtils.IndexOf($stdout, " FreeSpace"));
                                        $stdout = StringUtils.Substring($stdout,
                                            StringUtils.IndexOf($stdout, diskLabel) + StringUtils.Length(diskLabel));
                                        $stdout = StringUtils.Substring($stdout, 0, StringUtils.IndexOf($stdout, "\n"));
                                        $stdout = StringUtils.Remove($stdout, " ", "\r");
                                        diskSpace = StringUtils.ToInteger($stdout);
                                        estimatedSize = InstallerAppManager.APP_SIZE;
                                        loadProtocol(selectedBsp, onLoad);
                                    } else {
                                        ExceptionsManager.Throw(this.getClassName(), "Unable to get system drive");
                                    }
                                });
                            } else {
                                ExceptionsManager.Throw(this.getClassName(), "Unable to get disk space");
                            }
                        }
                    });
            };

            const showLicence : IEventsHandler = () : void => {
                const resourcesPath : string = appRootFolder + "/target/resource/data/Io/GpuSdk/Installer/Services";
                const licenseFolder : string = this.isVisionSDK ? "VisionSDK" : "GPUSDK";
                this.installDao.ConfigurationLibrary().fileSystem
                    .Read(resourcesPath + "/Licenses/" + licenseFolder + "/LICENSE.txt")
                    .Then(($data : string) : void => {
                        if (!ObjectValidator.IsEmptyOrNull($data)) {
                            let license : string = StringUtils.Replace(StringUtils.Replace($data, "  ",
                                StringUtils.Space(2)), "\n", StringUtils.NewLine());
                            $args.InstallationContentPanelArgs().LicenceAgreementPanelArgs().LicenceTextPanelArgs().ContentText(license);
                            this.installDao.ConfigurationLibrary().fileSystem
                                .Read(resourcesPath + "/Licenses/" + licenseFolder + "/SW-Content-Register.txt")
                                .Then(($data : string) : void => {
                                    const scr : string = StringUtils.Replace(
                                        StringUtils.Replace($data, "  ", StringUtils.Space(2)), "\n", StringUtils.NewLine());
                                    license +=
                                        StringUtils.NewLine() + StringUtils.NewLine() +
                                        $dao.getPageConfiguration().licenceAgreement.scrGlue + scr;
                                    $args.InstallationContentPanelArgs()
                                        .LicenceAgreementPanelArgs().LicenceTextPanelArgs().ContentText(license);
                                    installationContentInstance.licenceAgreementPanel.licenceText.content.Text(license);
                                });
                            installationContentInstance.licenceAgreementPanel.licenceText.content.Text(license);
                            installationContentInstance.licenceAgreementPanel.licenceCheckBox.Enabled(true);
                        } else if (this.getEnvironmentArgs().IsProductionMode()) {
                            ExceptionsManager.Throw(this.getClassName(), "Unable to get LICENSE text content correctly.");
                        } else {
                            installationContentInstance.licenceAgreementPanel.licenceText.content.Text("LICENSE text is empty!");
                        }
                    });
                installationContentInstance.nextButton.Enabled(false);
                $dao.SelectInstallationContentLayer(InstallationContentPanelType.LICENCE_AGREEMENT);
                model.setLayer(MainPageType.INSTALL_OR_CHANGE);
            };

            const infoBoxHandler : IEventsHandler = ($eventArgs : EventArgs) : void => {
                let text : string = "";
                switch ($eventArgs.Owner()) {
                case $instance.initialScreenPanel.installButton:
                    text = $dao.getPageConfiguration().initialScreen.infoBox.install;
                    break;
                case $instance.initialScreenPanel.uninstallButton:
                    text = $dao.getPageConfiguration().initialScreen.infoBox.uninstall;
                    break;
                case $instance.initialScreenPanel.repairButton:
                    text = $dao.getPageConfiguration().initialScreen.infoBox.repair;
                    break;
                case $instance.initialScreenPanel.switchButton:
                    text = $dao.getPageConfiguration().initialScreen.infoBox.switchBsp;
                    break;
                default:
                    text = "";
                    break;
                }
                $instance.initialScreenPanel.infoLabel.Text(text);
            };
            const assignInfoBoxEvents : any = ($button : Button) : void => {
                $button.getEvents().setOnMouseOver(infoBoxHandler);
                $button.getEvents().setOnFocus(infoBoxHandler);
            };
            assignInfoBoxEvents($instance.initialScreenPanel.installButton);
            assignInfoBoxEvents($instance.initialScreenPanel.uninstallButton);
            assignInfoBoxEvents($instance.initialScreenPanel.repairButton);
            assignInfoBoxEvents($instance.initialScreenPanel.switchButton);

            $instance.initialScreenPanel.installButton.getEvents().setOnClick(showLicence);
            $instance.initialScreenPanel.uninstallButton.getEvents().setOnClick(() : void => {
                isUninstall = true;
                this.appManager.getInstalledBSPVersion().Then(($protocolName : string) : void => {
                    $dao.SelectInstallationContentLayer(InstallationContentPanelType.UNINSTALLATION_PROCESS);
                    model.setLayer(MainPageType.UINSTALL);
                    loadProtocol($protocolName, () : void => {
                        installationProcessInstance.progressBar.GuiType(ProgressBarType.MARQUEE);
                        this.installDao.RunInstallationChain(InstallationProtocolType.UNINSTALL);
                    });
                });
            });
            $instance.initialScreenPanel.repairButton.getEvents().setOnClick(() : void => {
                isRepair = true;
                $dao.SelectInstallationContentLayer(InstallationContentPanelType.PRODUCT_SUMMARY);
                model.setLayer(MainPageType.REPAIR);
                this.appManager.getInstalledBSPVersion().Then(($value : string) : void => {
                    showProductSummary($value);
                });
            });
            $instance.initialScreenPanel.switchButton.getEvents().setOnClick(showLicence);

            if (isDesktop) {
                $instance.initialScreenPanel.cancelButton.getEvents().setOnClick(() : void => {
                    window.close();
                });
            }

            installationContentInstance.productScreenPanel.supportedBsp.getEvents().setOnChange(showProductSummary);
            installationContentInstance.productScreenPanel.legacyBsp.getEvents().setOnChange(showProductSummary);
            installationContentInstance.productScreenPanel.internalBsp.getEvents().setOnChange(showProductSummary);

            installationContentInstance.productSummaryPanel.selectAllCheckBox.getEvents().setOnClick(() : void => {
                const state : boolean = !installationContentInstance.productSummaryPanel.selectAllCheckBox.Value();
                installationContentInstance.productSummaryPanel.items.getChildElements()
                    .foreach(($item : InstallationItem) : void => {
                        if ($item.IsTypeOf(InstallationItem)) {
                            if (state) {
                                $item.Value(true);
                            } else if ($item.IsOptional() || requiredChain.indexOf($item) === -1) {
                                $item.Value(false);
                            }
                        }
                    });
            });

            installationContentInstance.logInPanel.password.getEvents().setOnChange(($eventArgs : KeyEventArgs) : void => {
                if ($eventArgs.IsTypeOf(KeyEventArgs) && $eventArgs.getKeyCode() === KeyMap.ENTER &&
                    installationContentInstance.nextButton.Enabled()) {
                    ElementManager.getElement(installationContentInstance.nextButton.Id() + "_Active").click();
                }
            });
            installationContentInstance.logInPanel.getEvents().setOnComplete(() : void => {
                const events : ElementEventsManager = new ElementEventsManager(
                    installationContentInstance.logInPanel, installationContentInstance.logInPanel.Id() + "_Policy");
                events.setOnClick(() : void => {
                    this.getHttpManager().ReloadTo($dao.getPageConfiguration().login.policyLink.url, true);
                });
                events.Subscribe();
            });

            const finishInstallation : any = () : void => {
                if (installationContentInstance.finalScreenPanel.Visible()) {
                    selectInstallationContentLayer(InstallationContentPanelType.CLOSING);
                    this.appManager
                        .Install(
                            appRootFolder,
                            StringUtils.Remove(StringUtils.Substring(this.installDao.getDaoDataSource(),
                                StringUtils.IndexOf(this.installDao.getDaoDataSource(), "/", false) + 1), ".jsonp", ".js"),
                            installationContentInstance.finalScreenPanel.desktopShortcutCheckBox.Value(),
                            installationContentInstance.finalScreenPanel.startMenuShortcutCheckBox.Value())
                        .Then(($status : boolean, $message? : string) : void => {
                            if ($status) {
                                if (isDesktop) {
                                    if (installationContentInstance.finalScreenPanel.openUserGuideCheckBox.Value()) {
                                        const openDoc : any = ($path : string) : void => {
                                            this.installDao.ConfigurationLibrary().fileSystem
                                                .Exists($path)
                                                .Then(($status : boolean) : void => {
                                                    if ($status) {
                                                        this.installDao.ConfigurationLibrary().terminal
                                                            .Execute("cmd", [
                                                                "/c", "\"start", "\"\"",
                                                                "\"" + StringUtils.Replace(StringUtils.Remove($path, ".lnk"),
                                                                    "/", "\\") + "\"\""
                                                            ])
                                                            .Then(() : void => {
                                                                window.close();
                                                            });
                                                    } else {
                                                        if (!this.isVisionSDK) {
                                                            LogIt.Error("Unable to find User Guide shortcut");
                                                        } else {
                                                            LogIt.Error("Unable to find Vision SDK Documentation");
                                                        }
                                                        window.close();
                                                    }
                                                });
                                        };
                                        if (this.isVisionSDK) {
                                            this.installDao.getConfigurationInstance().utils
                                                .getLocalAppDataPath(($path : string) : void => {
                                                    openDoc($path + "/VisionSDK/Docs/Vision SDK Documentation.pdf");
                                                });
                                        } else {
                                            this.installDao.getConfigurationInstance()
                                                .getInstallFolder(($path : string) : void => {
                                                    openDoc($path + "/Docs/User Guide.lnk");
                                                });
                                        }
                                    } else {
                                        window.close();
                                    }
                                }
                            } else {
                                ExceptionsManager.Throw(this.getClassName(), $message);
                            }
                        });
                }
            };

            installationContentInstance.nextButton.getEvents().setOnClick(() : void => {
                if (installationContentInstance.licenceAgreementPanel.Visible()) {
                    if (installationContentInstance.licenceAgreementPanel.licenceCheckBox.Value()) {
                        if (this.isVisionSDK) {
                            showProductSummary(visionSDKProfileName);
                        } else {
                            installationContentInstance.productScreenPanel.InstanceOwner().setLayer(
                                this.isInternal ? ProductScreenPanelType.INTERNAL : ProductScreenPanelType.PUBLIC);
                            selectInstallationContentLayer(InstallationContentPanelType.PRODUCT_SCREEN);
                        }
                    }
                } else if (installationContentInstance.productSummaryPanel.Visible() && !isLoggedIn) {
                    selectInstallationContentLayer(InstallationContentPanelType.LOGIN);
                    selectLogInContentLayer(LogInPanelType.DEFAULT);
                } else if (installationContentInstance.logInPanel.Visible() ||
                    installationContentInstance.productSummaryPanel.Visible() && isLoggedIn) {
                    selectLogInContentLayer(LogInPanelType.DEFAULT);
                    if (!isLoggedIn && isDesktop) {
                        selectLogInContentLayer(LogInPanelType.PROCESSING);
                        installationContentInstance.nextButton.Enabled(false);
                        installationContentInstance.backButton.Enabled(false);
                        windowHandler.Open("https://www.nxp.com/webapp", <WindowHandlerOpenOptions>{hidden: true})
                            .Then(($windowId : string, $url : string, $data : string) : void => {
                                if (StringUtils.Contains($data, "<title>NXP Sign In")) {
                                    windowHandler.ScriptExecute($windowId, <WindowHandlerScriptExecuteOptions>{
                                        script: "" +
                                            "document.getElementById(\"username\").value=" +
                                            "\"" + installationContentInstance.logInPanel.userName.Value() + "\";" +
                                            "document.getElementById(\"password\").value=" +
                                            "\"" + installationContentInstance.logInPanel.password.Value() + "\";" +
                                            "document.getElementsByName(\"loginbutton\")[0].click();"
                                    });
                                } else if (StringUtils.Contains($data, "<title>My Account Home Page</title>")) {
                                    windowHandler.Close($windowId);
                                    windowHandler.Open(loginRequestUrl, <WindowHandlerOpenOptions>{hidden: true})
                                        .Then(($windowId : string) : void => {
                                            windowHandler.getCookies($windowId).Then(($data : IWindowHandlerCookie[]) : void => {
                                                $data.forEach(($cookie : IWindowHandlerCookie) : void => {
                                                    if ($cookie.name === "JSESSIONID" && $cookie.path === "/webapp") {
                                                        (<IInstallationRecipe>this.installDao.getConfigurationInstance())
                                                            .casSession = $cookie.value;
                                                    }
                                                });
                                                windowHandler.Close($windowId);
                                                isLoggedIn = true;
                                                installationContentInstance.backButton.Enabled(true);
                                                selectInstallationContentLayer(InstallationContentPanelType.INSTALLATION_PROCESS);
                                                this.installDao.RunInstallationChain(InstallationProtocolType.INSTALL);
                                            });
                                        });
                                } else if (StringUtils.Contains($data, "name=\"I Agree\"")) {
                                    selectLogInContentLayer(LogInPanelType.NOT_ACCEPTED);
                                    this.installDao.ConfigurationLibrary().terminal.Open("https://www.nxp.com/security/login");
                                } else {
                                    selectLogInContentLayer(LogInPanelType.BAD_CREDENTIALS);
                                }
                            });
                    } else {
                        const chain : string[] = [];
                        let itemIndex : number = 0;
                        installationContentInstance.productSummaryPanel.items.getChildElements()
                            .foreach(($item : InstallationItem) : void => {
                                if ($item.IsTypeOf(InstallationItem)) {
                                    if ($item.Value() && !ObjectValidator.IsEmptyOrNull(fullChain[itemIndex])) {
                                        chain.push(fullChain[itemIndex]);
                                    }
                                    itemIndex++;
                                }
                            });
                        this.installDao.getStaticConfiguration().getChain = () : string[] => {
                            return chain;
                        };
                        selectInstallationContentLayer(InstallationContentPanelType.INSTALLATION_PROCESS);
                        this.installDao.RunInstallationChain(InstallationProtocolType.INSTALL);
                    }
                } else {
                    finishInstallation();
                }
            });
            if (isDesktop) {
                this.closeButton.getEvents().setOnClick(finishInstallation);
            }

            installationContentInstance.backButton.getEvents().setOnClick(() : void => {
                if (installationContentInstance.logInPanel.Visible()) {
                    showProductSummary(selectedBsp);
                } else if (installationProcessInstance.Visible()) {
                    this.installDao.Abort();
                    if (!isUninstall) {
                        showProductSummary(selectedBsp);
                    } else {
                        model.setLayer(MainPageType.INITIAL_SCREEN);
                    }
                } else if (installationContentInstance.productSummaryPanel.Visible()) {
                    if (isRepair) {
                        model.setLayer(MainPageType.INITIAL_SCREEN);
                    } else if (this.isVisionSDK) {
                        selectInstallationContentLayer(InstallationContentPanelType.LICENCE_AGREEMENT);
                    } else {
                        selectInstallationContentLayer(InstallationContentPanelType.PRODUCT_SCREEN);
                    }
                } else if (installationContentInstance.productScreenPanel.Visible()) {
                    selectInstallationContentLayer(InstallationContentPanelType.LICENCE_AGREEMENT);
                } else if (installationContentInstance.licenceAgreementPanel.Visible()) {
                    installationContentInstance.licenceAgreementPanel.licenceCheckBox.Value(false);
                    model.setLayer(MainPageType.INITIAL_SCREEN);
                }
            });

            installationProcessInstance.progressBar.getEvents().setOnChange(() : void => {
                if (installationProcessInstance.Visible()) {
                    if (installationProcessInstance.progressBar.Value() !== installationProcessInstance.progressBar.RangeEnd()) {
                        installationContentInstance.nextButton.Enabled(false);
                    }
                }
            });
        }

        protected onSuccess($instance : MainPage, $args : MainPageViewerArgs, $dao : MainPageDAO) : void {
            const model : MainPageViewer = this.getModel();

            $instance.initialScreenPanel.AnimateBar();
            if (this.isVisionSDK) {
                $instance.initialScreenPanel.IsVisionSDK(true);
                $instance.installationContentPanel.finalScreenPanel.desktopShortcutCheckBox.Visible(false);
                $instance.installationContentPanel.finalScreenPanel.startMenuShortcutCheckBox.Visible(false);
            }
            this.appManager = new InstallerAppManager(this.installDao, this.isVisionSDK);
            this.appManager.Selfinstall()
                .OnValidate(($status : boolean) : void => {
                    if ($status) {
                        $dao.SelectInstallationContentLayer(InstallationContentPanelType.SELFUPDATE);
                        model.setLayer(MainPageType.SELFUPDATE);
                    } else {
                        this.appManager.IsInstalled().Then(($status : boolean) : void => {
                            $instance.initialScreenPanel.InstanceOwner()
                                .setLayer($status ? InitialScreenPanelType.UNINSTALL_OR_CHANGE : InitialScreenPanelType.INSTALL);
                            const productArgs : ProductScreenPanelViewerArgs =
                                $dao.getModelArgs().InstallationContentPanelArgs().ProductScreenPanelArgs();
                            if ((productArgs.getSupportedBspList().Length() +
                                productArgs.getLegacyBspList().Length() +
                                productArgs.getInternalBspList().Length()) > 1 ||
                                this.isVisionSDK) {
                                $instance.initialScreenPanel.switchButton.Visible(false);
                                $instance.initialScreenPanel.CenterButtons();
                            }
                        });
                    }
                })
                .OnChange(($eventArgs : ProgressEventArgs) : void => {
                    if ($eventArgs.CurrentValue() !== -1) {
                        $instance.getEvents().FireAsynchronousMethod(() : void => {
                            $instance.installationContentPanel.selfupdatePanel.progressBar.Value(
                                Convert.ToFixed(100 / $eventArgs.RangeEnd() * $eventArgs.CurrentValue(), 0));
                            $instance.installationContentPanel.selfupdatePanel.progressBar.GuiType(ProgressBarType.GENERAL);
                        }, false);
                    } else {
                        $instance.getEvents().FireAsynchronousMethod(() : void => {
                            $instance.installationContentPanel.selfupdatePanel.progressBar.GuiType(ProgressBarType.MARQUEE);
                        }, false, 300);
                    }
                })
                .Then(($status : boolean) : void => {
                    if ($status) {
                        $instance.installationContentPanel.selfupdatePanel.progressBar.Value(100);
                    }
                });
        }

        protected resolver() : void {
            WindowManager.getEvents().setOnBlur(() : void => {
                ElementManager.setCssProperty("Browser", "background-color", "transparent");
            });

            if (this.isVisionSDK) {
                this.getDao().setConfigurationPath(this.hubLocation + "/Configuration/" +
                    this.getEnvironmentArgs().getProjectName() + "/VisionSDKLocalization");
            } else if (!this.isInternal) {
                this.getDao().setConfigurationPath(this.hubLocation + "/Configuration/" +
                    this.getEnvironmentArgs().getProjectName() + "/MainPageLocalization");
            }

            super.resolver();
        }

        protected getEnvironmentArgs() : EnvironmentArgs {
            return <EnvironmentArgs>super.getEnvironmentArgs();
        }
    }
}
