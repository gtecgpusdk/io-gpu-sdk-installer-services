/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Io.GpuSdk.Installer.Services.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IInstallationRecipe = Io.GpuSdk.Installer.Services.Interfaces.DAO.IInstallationRecipe;
    import InstallationProtocolType = Com.Wui.Framework.Services.Enums.InstallationProtocolType;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import InstallerAppManager = Io.GpuSdk.Installer.Services.Utils.InstallerAppManager;
    import ProgressEventArgs = Com.Wui.Framework.Commons.Events.Args.ProgressEventArgs;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import LogInPanelType = Io.GpuSdk.Installer.Gui.BaseInterface.Enums.Panels.LogInPanelType;
    import ElementEventsManager = Com.Wui.Framework.Gui.Events.ElementEventsManager;
    import WindowHandlerConnector = Com.Wui.Framework.Services.Connectors.WindowHandlerConnector;
    import WindowHandlerOpenOptions = Com.Wui.Framework.Services.Connectors.WindowHandlerOpenOptions;
    import WindowHandlerScriptExecuteOptions = Com.Wui.Framework.Services.Connectors.WindowHandlerScriptExecuteOptions;
    import IWindowHandlerCookie = Com.Wui.Framework.Services.Connectors.IWindowHandlerCookie;
    import ObjectDecoder = Com.Wui.Framework.Commons.Utils.ObjectDecoder;
    import IRuntimeTestPromise = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.IRuntimeTestPromise;

    export class InstallationProcessTest extends Com.Wui.Framework.Services.RuntimeTests.InstallationProcessTest {
        private selectedProtocol : InstallationProtocolType;

        constructor() {
            super();
            // this.setScript("resource/data/Io/GpuSdk/Installer/Services/Configuration/Windows7.jsonp");
            // this.setScript("https://localhost.wuiframework.com/com-wui-framework-rest-services/Download/Windows7.jsonp");
            // this.setScript("https://hub.wuiframework.com/Download/Windows7.jsonp");
            // this.setScript("resource/data/Io/GpuSdk/Installer/Services/Configuration/L4.1.30_8DV_Beta.jsonp");
            // this.setScript("resource/data/Io/GpuSdk/Installer/Services/Configuration/L4.1.30.jsonp");
            // this.setScript("https://localhost.wuiframework.com/com-wui-framework-rest-services/" +
            //     "Configuration/io-gpu-sdk-installer-services/L4.1.30");
            this.setScript("resource/data/Io/GpuSdk/Installer/Services/Configuration/VisionSDKStudio.jsonp");

            this.setChain([
                // "PortableCMake",
                // "Msys2",
                "VisionSDKStudio",
                "VisionSDKStudioDocs"
            ], true);
        }

        public testInstallerAppManager() : void {
            this.addButton("Force install", () : void => {
                const manager : InstallerAppManager = new InstallerAppManager(this.getDao());
                manager.Install(
                    StringUtils.Remove(ObjectDecoder.Url(this.getRequest().getHostUrl()),
                        "file:///", "file://", "/target/index.html"), "L4.1.30_8DV_Beta")
                    .Then(($status : boolean, $message? : string) : void => {
                        if ($status) {
                            Echo.Printf("Installer successfully installed");
                        } else {
                            Echo.Printf($message);
                        }
                    });
            });

            this.addButton("Force uninstall", () : void => {
                const manager : InstallerAppManager = new InstallerAppManager(this.getDao());
                manager.Uninstall().Then(($status : boolean, $message? : string) : void => {
                    if ($status) {
                        Echo.Printf("Installer successfully uninstalled");
                    } else {
                        Echo.Printf($message);
                    }
                });
            });

            this.addButton("Install/Uninstall", () : void => {
                const manager : InstallerAppManager = new InstallerAppManager(this.getDao());
                manager.IsInstalled().Then(($status : boolean) : void => {
                    if ($status) {
                        manager.Uninstall().Then(($status : boolean, $message? : string) : void => {
                            if ($status) {
                                Echo.Printf("Installer successfully uninstalled");
                            } else {
                                Echo.Printf($message);
                            }
                        });
                    } else {
                        manager.Install(
                            StringUtils.Remove(ObjectDecoder.Url(this.getRequest().getHostUrl()),
                                "file:///", "file://", "/target/index.html"), "L4.1.30_8DV_Beta")
                            .Then(($status : boolean, $message? : string) : void => {
                                if ($status) {
                                    Echo.Printf("Installer successfully installed");
                                } else {
                                    Echo.Printf($message);
                                }
                            });
                    }
                });
            });

            this.addButton("Selfupdate", () : void => {
                const manager : InstallerAppManager = new InstallerAppManager(this.getDao());
                manager.Selfinstall()
                    .OnValidate(($status : boolean) : void => {
                        if (!$status) {
                            Echo.Printf("Already up to date.");
                        }
                    })
                    .OnChange(($eventArgs : ProgressEventArgs) : void => {
                        ElementManager.setInnerHtml("ScriptProcess", StringUtils.Format("<b>Get selfinstall package:</b> {0}/{1}",
                            $eventArgs.CurrentValue() + "", $eventArgs.RangeEnd() + ""));
                        window.scrollTo(0, ElementManager.getElement("ScriptProcess").offsetTop);
                    })
                    .Then(($status : boolean, $message? : string) : void => {
                        WindowManager.ScrollToBottom();
                        if ($status) {
                            Echo.Printf("Selfinstall process has finished successfully.");
                        } else {
                            Echo.Printf($message);
                        }
                    });
            });
        }

        protected before() : IRuntimeTestPromise {
            const promise : IRuntimeTestPromise = super.before();
            Echo.Println("<style>body {overflow: auto;}</style>");
            Echo.Println(
                "<div id=\"logIn\" style=\"display: none;\">" +
                "<h3>NXP Login credentials</h3>" +
                "UserName: <input id=\"testUserName\" type=\"text\"> Pass: <input id=\"testUserPass\" type=\"password\"></br>" +
                "<div id=\"Continue\" style=\"border: 1px solid red; cursor: pointer; " +
                "width: 250px; overflow-x: hidden; overflow-y: hidden; " +
                "text-align: center; font-size: 16px; font-family: Consolas; color: red;\">Continue with install process</div>" +
                "</div>");
            const loginEvents : ElementEventsManager = new ElementEventsManager("Continue");
            let loginRequestUrl : string;
            loginEvents.setOnClick(() : void => {
                const windowHandler : WindowHandlerConnector = new WindowHandlerConnector();
                windowHandler.Open("https://www.nxp.com/webapp", <WindowHandlerOpenOptions>{hidden: true})
                    .Then(($windowId : string, $url : string, $data : string) : void => {
                        if (StringUtils.Contains($data, "<title>NXP Sign In")) {
                            windowHandler.ScriptExecute($windowId, <WindowHandlerScriptExecuteOptions>{
                                script: "" +
                                "document.getElementById(\"username\").value=" +
                                "\"" + (<HTMLInputElement>ElementManager.getElement("testUserName")).value + "\";" +
                                "document.getElementById(\"password\").value=" +
                                "\"" + (<HTMLInputElement>ElementManager.getElement("testUserPass")).value + "\";" +
                                "document.getElementsByName(\"loginbutton\")[0].click();"
                            });
                        } else if (StringUtils.Contains($data, "<title>My Account Home Page</title>")) {
                            windowHandler.Close($windowId);
                            windowHandler.Open(loginRequestUrl, <WindowHandlerOpenOptions>{hidden: true})
                                .Then(($windowId : string) : void => {
                                    windowHandler.getCookies($windowId).Then(($data : IWindowHandlerCookie[]) : void => {
                                        $data.forEach(($cookie : IWindowHandlerCookie) : void => {
                                            if ($cookie.name === "JSESSIONID" && $cookie.path === "/webapp") {
                                                const instance : IInstallationRecipe =
                                                    <IInstallationRecipe>this.getDao().getConfigurationInstance();
                                                instance.casSession = $cookie.value;
                                                ElementManager.Hide("logIn");
                                                WindowManager.ScrollToBottom();
                                                Echo.Printf("CAS session has been set to: {0}", instance.casSession);
                                                this.runProtocol(this.selectedProtocol);
                                            }
                                        });
                                        windowHandler.Close($windowId);
                                    });
                                });
                        }
                    });
            });
            loginEvents.Subscribe();
            this.getDao().getEvents().setOnStart(() : void => {
                const instance : IInstallationRecipe = <IInstallationRecipe>this.getDao().getConfigurationInstance();
                instance.casSession = "qy7vwqc9YXCOFkW1ivIqPDp-.ebiz_ms1";
                instance.onLoginRequest = ($type : LogInPanelType, $where : string) : void => {
                    this.getDao().Abort();
                    if ($type === LogInPanelType.BAD_CREDENTIALS) {
                        Echo.Printf("log in required");
                        ElementManager.Show("logIn");
                        this.getEventsManager().FireAsynchronousMethod(() : void => {
                            window.scrollTo(0, ElementManager.getElement("logIn").offsetTop);
                        }, 300);
                        loginRequestUrl = $where;
                    } else if ($type === LogInPanelType.NOT_ACCEPTED) {
                        Echo.Printf("Terms of use not accepted");
                    } else {
                        Echo.Printf("System outage");
                    }
                };
            });
            return promise;
        }

        protected runProtocol($type : InstallationProtocolType) : void {
            this.selectedProtocol = $type;
            super.runProtocol($type);
        }
    }
}
/* dev:end */
