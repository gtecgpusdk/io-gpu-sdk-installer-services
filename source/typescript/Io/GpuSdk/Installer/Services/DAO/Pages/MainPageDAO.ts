/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.GpuSdk.Installer.Services.DAO.Pages {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import MainPageViewerArgs = Io.GpuSdk.Installer.Gui.BaseInterface.ViewersArgs.Pages.MainPageViewerArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IMainPageLocalization = Io.GpuSdk.Installer.Services.Interfaces.DAO.IMainPageLocalization;
    import InitialScreenPanelViewerArgs = Io.GpuSdk.Installer.Gui.BaseInterface.ViewersArgs.Panels.InitialScreenPanelViewerArgs;
    import ProductScreenPanelViewerArgs = Io.GpuSdk.Installer.Gui.BaseInterface.ViewersArgs.Panels.ProductScreenPanelViewerArgs;
    import InstallationContentPanelViewerArgs = Io.GpuSdk.Installer.Gui.BaseInterface.ViewersArgs.Panels.InstallationContentPanelViewerArgs;
    import LicenceAgreementPanelViewerArgs = Io.GpuSdk.Installer.Gui.BaseInterface.ViewersArgs.Panels.LicenceAgreementPanelViewerArgs;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import FinalScreenPanelViewerArgs = Io.GpuSdk.Installer.Gui.BaseInterface.ViewersArgs.Panels.FinalScreenPanelViewerArgs;
    import InstallationContentPanelType = Io.GpuSdk.Installer.Gui.BaseInterface.Enums.Panels.InstallationContentPanelType;
    import LogInPanelViewerArgs = Io.GpuSdk.Installer.Gui.BaseInterface.ViewersArgs.Panels.LogInPanelViewerArgs;
    import LogInPanelType = Io.GpuSdk.Installer.Gui.BaseInterface.Enums.Panels.LogInPanelType;
    import ProductSummaryPanelViewerArgs = Io.GpuSdk.Installer.Gui.BaseInterface.ViewersArgs.Panels.ProductSummaryPanelViewerArgs;

    export class MainPageDAO extends BasePageDAO {
        public static defaultConfigurationPath : string =
            "resource/data/Io/GpuSdk/Installer/Services/Localization/MainPageLocalization.jsonp";

        public getPageConfiguration() : IMainPageLocalization {
            return <IMainPageLocalization>super.getStaticConfiguration();
        }

        public getModelArgs() : MainPageViewerArgs {
            if (!ObjectValidator.IsSet(this.modelArgs)) {
                const pageConfiguration : IMainPageLocalization = this.getPageConfiguration();
                const args : MainPageViewerArgs = new MainPageViewerArgs();

                const initialScreenArgs : InitialScreenPanelViewerArgs = args.InitialScreenPanelArgs();
                initialScreenArgs.InstallButtonText(pageConfiguration.initialScreen.buttons.install);
                initialScreenArgs.CancelButtonText(pageConfiguration.initialScreen.buttons.cancel);
                initialScreenArgs.UninstallButtonText(pageConfiguration.initialScreen.buttons.uninstall);
                initialScreenArgs.RepairButtonText(pageConfiguration.initialScreen.buttons.repair);
                initialScreenArgs.SwitchButtonText(pageConfiguration.initialScreen.buttons.switchBsp);
                initialScreenArgs.InfoLabelText(pageConfiguration.initialScreen.infoBox.install);

                const installationContentArgs : InstallationContentPanelViewerArgs = args.InstallationContentPanelArgs();
                installationContentArgs.NextButtonText(pageConfiguration.buttons.next);
                installationContentArgs.BackButtonText(pageConfiguration.buttons.back);

                const licenceAgreementArgs : LicenceAgreementPanelViewerArgs = installationContentArgs.LicenceAgreementPanelArgs();
                licenceAgreementArgs.LicenceTextPanelArgs().ContentText(
                    StringUtils.Replace(pageConfiguration.licenceAgreement.licence, "\n", StringUtils.NewLine()));
                licenceAgreementArgs.LicenceCheckBoxText(pageConfiguration.licenceAgreement.checkBox);

                const productScreenArgs : ProductScreenPanelViewerArgs = installationContentArgs.ProductScreenPanelArgs();
                productScreenArgs.SupportedBspHeader(pageConfiguration.productScreen.supported.title);
                productScreenArgs.SupportedBspDescription(pageConfiguration.productScreen.supported.description);
                pageConfiguration.productScreen.supported.items.forEach(($item : string) : void => {
                    productScreenArgs.AddSupportedBsp($item);
                });
                productScreenArgs.LegacyBspHeader(pageConfiguration.productScreen.legacy.title);
                productScreenArgs.LegacyBspDescription(pageConfiguration.productScreen.legacy.description);
                pageConfiguration.productScreen.legacy.items.forEach(($item : string) : void => {
                    productScreenArgs.AddLegacyBsp($item);
                });
                productScreenArgs.InternalBspHeader(pageConfiguration.productScreen.internal.title);
                productScreenArgs.InternalBspDescription(pageConfiguration.productScreen.internal.description);
                pageConfiguration.productScreen.internal.items.forEach(($item : string) : void => {
                    productScreenArgs.AddInternalBsp($item);
                });

                const productSummaryArgs : ProductSummaryPanelViewerArgs = installationContentArgs.ProductSummaryPanelArgs();
                productSummaryArgs.SizeText(pageConfiguration.productSummary.estimateSize);
                productSummaryArgs.SelectAllText(pageConfiguration.productSummary.selectAll);
                productSummaryArgs.NotifyText(pageConfiguration.productSummary.notifications.not_enough_disk_space);

                installationContentArgs.ChangeDirectoryPanelArgs().LocationWindow(pageConfiguration.changeDirectory.defaultPath);
                installationContentArgs.InstallationProcessPanelArgs().ProgressText(pageConfiguration.installationProcess.status);

                const finalScreenArgs : FinalScreenPanelViewerArgs = installationContentArgs.FinalScreenPanelArgs();
                finalScreenArgs.InstallationStatus(pageConfiguration.finalScreen.status);
                finalScreenArgs.DesktopShortcutCheckBoxText(pageConfiguration.finalScreen.desktopShortcut);
                finalScreenArgs.StartMenuCheckBoxText(pageConfiguration.finalScreen.startMenuShortcut);
                finalScreenArgs.UserGuideCheckBoxText(pageConfiguration.finalScreen.openUserGuideShortcut);

                const logInArgs : LogInPanelViewerArgs = installationContentArgs.LogInPanelArgs();
                logInArgs.UserNameHint(pageConfiguration.login.username);
                logInArgs.UserNameTooltip(pageConfiguration.login.tooltip);
                logInArgs.PasswordHint(pageConfiguration.login.password);
                logInArgs.Status(pageConfiguration.login.notifications.processing_login);
                logInArgs.InfoLabelText(pageConfiguration.login.infoBox);
                logInArgs.PolicyLinkText(pageConfiguration.login.policyLink.text);

                this.modelArgs = args;
            }
            return <MainPageViewerArgs>this.modelArgs;
        }

        public SelectInstallationContentLayer($layer : InstallationContentPanelType) : void {
            const args : InstallationContentPanelViewerArgs = this.getModelArgs().InstallationContentPanelArgs();
            const pageConfiguration : IMainPageLocalization = this.getPageConfiguration();
            args.BackButtonText(pageConfiguration.buttons.back);
            args.NextButtonText(pageConfiguration.buttons.next);
            switch ($layer) {
            case InstallationContentPanelType.LICENCE_AGREEMENT:
                args.ContentTitle(pageConfiguration.licenceAgreement.title);
                break;
            case InstallationContentPanelType.PRODUCT_SCREEN:
                args.ContentTitle(pageConfiguration.productScreen.title);
                break;
            case InstallationContentPanelType.PRODUCT_SUMMARY:
                args.ContentTitle(pageConfiguration.productSummary.title);
                break;
            case InstallationContentPanelType.INSTALLATION_PROCESS:
                args.ContentTitle(pageConfiguration.installationProcess.title);
                args.BackButtonText(pageConfiguration.buttons.cancel);
                break;
            case InstallationContentPanelType.UNINSTALLATION_PROCESS:
                args.ContentTitle(pageConfiguration.uninstallProcess.title);
                args.BackButtonText(pageConfiguration.buttons.cancel);
                break;
            case InstallationContentPanelType.FINAL_SCREEN:
            case InstallationContentPanelType.CLOSING:
                args.ContentTitle(pageConfiguration.finalScreen.title);
                args.NextButtonText(pageConfiguration.buttons.finish);
                break;
            case InstallationContentPanelType.CHANGE_DIRECTORY:
                args.ContentTitle(pageConfiguration.changeDirectory.title);
                break;
            case InstallationContentPanelType.LOGIN:
                args.ContentTitle(pageConfiguration.login.title);
                break;
            case InstallationContentPanelType.SELFUPDATE:
                args.ContentTitle(pageConfiguration.selfupdate.title);
                break;
            default:
                break;
            }
        }

        public SelectLogInContentLayer($layer : LogInPanelType) : void {
            const args : LogInPanelViewerArgs = this.getModelArgs().InstallationContentPanelArgs().LogInPanelArgs();
            const pageConfiguration : IMainPageLocalization = this.getPageConfiguration();
            switch ($layer) {
            case LogInPanelType.BAD_CREDENTIALS:
                args.Message(pageConfiguration.login.notifications.bad_user_name_or_password);
                break;
            case LogInPanelType.NOT_ACCEPTED:
                args.Message(pageConfiguration.login.notifications.terms_of_use_not_accepted);
                break;
            case LogInPanelType.SYSTEM_OUTAGE:
                args.Message(pageConfiguration.login.notifications.unable_to_log_into_the_nxp_web_due_to_system_outage);
                break;
            default:
                break;
            }
        }
    }
}
