/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.GpuSdk.Installer.Services.DAO.Pages {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IConnectionLostPageLocalization = Io.GpuSdk.Installer.Services.Interfaces.DAO.IConnectionLostPageLocalization;
    import ConnectionLostPageViewerArgs = Io.GpuSdk.Installer.Gui.BaseInterface.ViewersArgs.Pages.ConnectionLostPageViewerArgs;

    export class ConnectionLostPageDAO extends BasePageDAO {
        public static defaultConfigurationPath : string =
            "resource/data/Io/GpuSdk/Installer/Services/Localization/ConnectionLostPageLocalization.jsonp";

        public getPageConfiguration() : IConnectionLostPageLocalization {
            return <IConnectionLostPageLocalization>super.getStaticConfiguration();
        }

        public getModelArgs() : ConnectionLostPageViewerArgs {
            if (!ObjectValidator.IsSet(this.modelArgs)) {
                const pageConfiguration : IConnectionLostPageLocalization = this.getPageConfiguration();
                const args : ConnectionLostPageViewerArgs = new ConnectionLostPageViewerArgs();
                args.HeaderText(pageConfiguration.header);
                args.StatusText(pageConfiguration.status);
                args.ExitButtonText(pageConfiguration.exitButton);

                this.modelArgs = args;
            }
            return <ConnectionLostPageViewerArgs>this.modelArgs;
        }
    }
}
