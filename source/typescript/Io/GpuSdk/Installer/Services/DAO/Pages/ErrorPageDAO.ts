/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.GpuSdk.Installer.Services.DAO.Pages {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import IErrorPageLocalization = Io.GpuSdk.Installer.Services.Interfaces.DAO.IErrorPageLocalization;
    import ErrorPageViewerArgs = Io.GpuSdk.Installer.Gui.BaseInterface.ViewersArgs.Pages.ErrorPageViewerArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ErrorPageType = Io.GpuSdk.Installer.Gui.BaseInterface.Enums.Pages.ErrorPageType;

    export class ErrorPageDAO extends BasePageDAO {
        public static defaultConfigurationPath : string =
            "resource/data/Io/GpuSdk/Installer/Services/Localization/ErrorPageLocalization.jsonp";

        public getPageConfiguration() : IErrorPageLocalization {
            return <IErrorPageLocalization>super.getStaticConfiguration();
        }

        public getModelArgs() : ErrorPageViewerArgs {
            if (!ObjectValidator.IsSet(this.modelArgs)) {
                const pageConfiguration : IErrorPageLocalization = this.getPageConfiguration();
                const args : ErrorPageViewerArgs = new ErrorPageViewerArgs();
                args.HeaderText(pageConfiguration.header);
                args.StatusText(pageConfiguration.notifications.collecting_crash_report_data);
                args.SupportText(pageConfiguration.support.text);
                args.SupportLinkText(pageConfiguration.support.link.text);
                args.SeeLogButtonText(pageConfiguration.buttons.log);
                args.ExitButtonText(pageConfiguration.buttons.exit);

                this.modelArgs = args;
            }
            return <ErrorPageViewerArgs>this.modelArgs;
        }

        public SelectNotificationFor($layer : ErrorPageType) : void {
            const pageConfiguration : IErrorPageLocalization = this.getPageConfiguration();
            const args : ErrorPageViewerArgs = this.getModelArgs();
            switch ($layer) {
            case ErrorPageType.DEFAULT:
            case ErrorPageType.INITIAL_SCREEN:
                args.StatusText(pageConfiguration.notifications.collecting_crash_report_data);
                break;
            case ErrorPageType.SENDING_REPORT:
                args.StatusText(pageConfiguration.notifications.sending_crash_report);
                break;
            case ErrorPageType.REPORT_FAIL:
                args.StatusText(pageConfiguration.notifications
                    .we_are_sorry__installation_was_not_finished_successfully_and_we_were_unable_to_send_a_crash_report);
                break;
            case ErrorPageType.MANUAL_REPORT:
                args.StatusText(pageConfiguration.notifications
                    .we_are_sorry__installation_was_not_finished_successfully_and_we_were_unable_to_send_a_crash_report);
                args.SupportText(pageConfiguration.manualReport.text);
                args.SupportLinkText(pageConfiguration.manualReport.link.text);
                break;
            case ErrorPageType.REPORT_OK:
                args.StatusText(pageConfiguration.notifications.report_was_sent_successfully);
                break;
            default:
                break;
            }
        }
    }
}
