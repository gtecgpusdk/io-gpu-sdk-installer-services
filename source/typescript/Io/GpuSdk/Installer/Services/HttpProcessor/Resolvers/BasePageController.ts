/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.GpuSdk.Installer.Services.HttpProcessor.Resolvers {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import BasePageDAO = Io.GpuSdk.Installer.Services.DAO.Pages.BasePageDAO;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import IconType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.IconType;
    import ImageButtonType = Io.VisionSDK.UserControls.BaseInterface.Enums.UserControls.ImageButtonType;
    import Icon = Io.VisionSDK.UserControls.BaseInterface.UserControls.Icon;
    import ImageButton = Io.VisionSDK.UserControls.BaseInterface.UserControls.ImageButton;
    import Label = Io.VisionSDK.UserControls.BaseInterface.UserControls.Label;
    import ResizeBar = Com.Wui.Framework.UserControls.BaseInterface.Components.ResizeBar;
    import DragBar = Com.Wui.Framework.UserControls.BaseInterface.Components.DragBar;
    import EventsManager = Com.Wui.Framework.UserControls.Events.EventsManager;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;

    /**
     * BasePageController class provides handling of requests for controller.
     */
    export class BasePageController extends Com.Wui.Framework.Services.HttpProcessor.Resolvers.BasePageController {

        constructor() {
            super();

            this.setPageIconSource("resource/graphics/GpuSdk.ico");
            this.loaderIcon.IconType(IconType.SPINNER_WHITE_BIG);
            this.appIcon.Visible(false);
            this.appTitle.Visible(false);
            this.minimizeButton.GuiType(ImageButtonType.APP_MINIMIZE);
            this.maximizeButton.Visible(false);
            this.closeButton.GuiType(ImageButtonType.APP_CLOSE);
        }

        protected argsHandler($GET : ArrayList<string>) : void {
            let prefix : string;
            if ($GET.KeyExists("type") && $GET.getItem("type") === "VisionSDK" || $GET.Contains("isVision")) {
                prefix = "Vision";
            } else {
                prefix = "GPU";
            }
            this.setPageTitle(prefix + " SDK Installer v" + this.getEnvironmentArgs().getProjectVersion());
        }

        /**
         * @return {IIcon} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.UserControls.IIcon
         */
        protected getLoaderIconClass() : any {
            return Icon;
        }

        /**
         * @return {ILabel} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.UserControls.ILabel
         */
        protected getLoaderTextClass() : any {
            return Label;
        }

        /**
         * @return {ImageButton} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.UserControls.ImageButton
         */
        protected getAppImageButtonClass() : any {
            return ImageButton;
        }

        /**
         * @return {IIcon} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.UserControls.IIcon
         */
        protected getAppIconClass() : any {
            return Icon;
        }

        /**
         * @return {ILabel} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.UserControls.ILabel
         */
        protected getAppTitleTextClass() : any {
            return Label;
        }

        /**
         * @return {IResizeBar} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IResizeBar
         */
        protected getAppResizeBarClass() : any {
            return ResizeBar;
        }

        /**
         * @return {IDragBar} This method should return class with
         * interface Com.Wui.Framework.Gui.Interfaces.Components.IDragBar
         */
        protected getAppDragBarClass() : any {
            return DragBar;
        }

        protected setDao($value : BasePageDAO) : void {
            super.setDao($value);
        }

        protected getDao() : BasePageDAO {
            return <BasePageDAO>super.getDao();
        }

        protected getCssInterfaceName() : string {
            return StringUtils.Remove(BasePageController.ClassName(), ".") + " " +
                StringUtils.Remove(Com.Wui.Framework.Services.HttpProcessor.Resolvers.BasePageController.ClassName(), ".");
        }

        protected getEventsManager() : EventsManager {
            return <EventsManager>super.getEventsManager();
        }
    }
}
