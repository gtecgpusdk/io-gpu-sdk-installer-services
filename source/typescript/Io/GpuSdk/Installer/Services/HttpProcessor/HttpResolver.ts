/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.GpuSdk.Installer.Services.HttpProcessor {
    "use strict";
    import HttpRequestConstants = Com.Wui.Framework.Gui.Enums.HttpRequestConstants;
    import EventsManager = Com.Wui.Framework.UserControls.Events.EventsManager;
    import HttpManager = Com.Wui.Framework.Services.HttpProcessor.HttpManager;

    export class HttpResolver extends Com.Wui.Framework.Services.HttpProcessor.HttpResolver {

        public getEvents() : EventsManager {
            return <EventsManager>super.getEvents();
        }

        protected getStartupResolvers() : any {
            const resolvers : any = super.getStartupResolvers();
            resolvers["/"] = Io.GpuSdk.Installer.Services.Controllers.Pages.MainPageController;
            resolvers["/Release/{type}"] = Io.GpuSdk.Installer.Services.Controllers.Pages.MainPageController;
            resolvers["/ServerError/Http/ConnectionLost"] =
                Io.GpuSdk.Installer.Services.Controllers.Pages.ConnectionLostPageController;

            if (!Loader.getInstance().getEnvironmentArgs().IsProductionMode()) {
                resolvers["/web/"] = Index;
                resolvers["/index"] = Index;
                resolvers["/about/Package"] = Io.GpuSdk.Installer.Services.HttpProcessor.Resolvers.AboutPackage;
                resolvers["/InstallationProcessTest"] = Io.GpuSdk.Installer.Services.RuntimeTests.InstallationProcessTest;
            } else {
                resolvers["/web/"] = null;
                resolvers["/index"] = null;
                resolvers["/ServerError/Exception/{" + HttpRequestConstants.EXCEPTION_TYPE + "}"] =
                    Io.GpuSdk.Installer.Services.Controllers.Pages.ErrorPageController;
                resolvers["/ServerError/Http/NotFound"] = Io.GpuSdk.Installer.Services.Controllers.Pages.ErrorPageController;
                resolvers["/ServerError/Http/DefaultPage"] = Io.GpuSdk.Installer.Services.Controllers.Pages.ErrorPageController;
            }
            return resolvers;
        }

        protected getHttpManagerClass() : any {
            return HttpManager;
        }

        protected getEventsManagerClass() : any {
            return EventsManager;
        }
    }
}
