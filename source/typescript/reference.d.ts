/// <reference path="../../dependencies/com-wui-framework-commons/source/typescript/reference.d.ts" />
/// <reference path="../../dependencies/com-wui-framework-gui/source/typescript/reference.d.ts" />
/// <reference path="../../dependencies/com-wui-framework-services/source/typescript/reference.d.ts" />
/// <reference path="../../dependencies/com-wui-framework-usercontrols/source/typescript/reference.d.ts" />
/// <reference path="../../dependencies/io-vision-sdk-usercontrols/source/typescript/reference.d.ts" />
/// <reference path="../../dependencies/io-gpu-sdk-installer-gui/source/typescript/reference.d.ts" />

// generated-code-start
/// <reference path="interfacesMap.ts" />
/// <reference path="Io/GpuSdk/Installer/Services/Interfaces/IProject.ts" />
/// <reference path="Io/GpuSdk/Installer/Services/DAO/Resources.ts" />
/// <reference path="Io/GpuSdk/Installer/Services/Interfaces/DAO/IMainPageLocalization.ts" />
/// <reference path="Io/GpuSdk/Installer/Services/Interfaces/DAO/IErrorPageLocalization.ts" />
/// <reference path="Io/GpuSdk/Installer/Services/Interfaces/DAO/IConnectionLostPageLocalization.ts" />
/// <reference path="Io/GpuSdk/Installer/Services/EnvironmentArgs.ts" />
/// <reference path="Io/GpuSdk/Installer/Services/DAO/Pages/BasePageDAO.ts" />
/// <reference path="Io/GpuSdk/Installer/Services/HttpProcessor/Resolvers/AboutPackage.ts" />
/// <reference path="Io/GpuSdk/Installer/Services/Interfaces/DAO/IInstallationRecipe.ts" />
/// <reference path="Io/GpuSdk/Installer/Services/DAO/Pages/ConnectionLostPageDAO.ts" />
/// <reference path="Io/GpuSdk/Installer/Services/DAO/Pages/ErrorPageDAO.ts" />
/// <reference path="Io/GpuSdk/Installer/Services/HttpProcessor/Resolvers/BasePageController.ts" />
/// <reference path="Io/GpuSdk/Installer/Services/DAO/Pages/MainPageDAO.ts" />
/// <reference path="Io/GpuSdk/Installer/Services/Utils/InstallerAppManager.ts" />
/// <reference path="Io/GpuSdk/Installer/Services/Controllers/Pages/ConnectionLostPageController.ts" />
/// <reference path="Io/GpuSdk/Installer/Services/Controllers/Pages/ErrorPageController.ts" />
/// <reference path="Io/GpuSdk/Installer/Services/RuntimeTests/InstallationProcessTest.ts" />
/// <reference path="Io/GpuSdk/Installer/Services/Controllers/Pages/MainPageController.ts" />
// generated-code-end

/// <reference path="Io/GpuSdk/Installer/Services/HttpProcessor/HttpResolver.ts" />
/// <reference path="Io/GpuSdk/Installer/Services/Index.ts" />
/// <reference path="Io/GpuSdk/Installer/Services/Loader.ts" />
/// <reference path="Io/GpuSdk/Installer/Services/Utils/FallbackMappings.ts" />
