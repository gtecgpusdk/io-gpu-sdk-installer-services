/** WARNING: this file has been automatically generated from typescript interfaces, which exist in this package. */

/* tslint:disable: variable-name no-use-before-declare only-arrow-functions */
namespace Io.GpuSdk.Installer.Services.Utils {
    "use strict";
    export let IInstallUnistallPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Io.GpuSdk.Installer.Services.Utils {
    "use strict";
    export let ISelfinstallPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "OnValidate",
                "OnChange",
                "Then"
            ]);
        }();
}

namespace Io.GpuSdk.Installer.Services.Utils {
    "use strict";
    export let IInstalledPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Io.GpuSdk.Installer.Services.Utils {
    "use strict";
    export let IBSPVersionPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

/* tslint:enable */
